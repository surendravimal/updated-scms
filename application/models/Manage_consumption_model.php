<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_consumption_model extends CI_Model {
	public function __construct(){
		$this->load->database();
	}

	function getActiveProducts(){
		$sIndexColumn = "siteProductId";
		$sTable = "(SELECT siteProductId,siteInChargeId,productId,SUM(issuedQuantity) issuedQuantity,pricePerUnit,(SUM(issuedQuantity)*pricePerUnit) totalPrice,insertDate,updateDate FROM( SELECT * FROM site_products ORDER BY siteProductId ASC) sp WHERE sp.siteInChargeId=".$this->session->userdata('userId')." GROUP BY sp.siteInChargeId,sp.productId ORDER BY sp.updateDate DESC ) ssp";
		
			$sWhere ="inner join products p on p.productId= ssp.productId
					left join major_category mjc on mjc.majorCateId = p.majorCateId
					left join minor_category mnc on mnc.minorCateId = p.minorCateId
					left join mini_category minc on minc.miniCateId = p.miniCateId";
		
		

		$aColumns = array( 'ssp.siteProductId','ssp.productId','ssp.issuedQuantity','p.measuringUnit','ssp.pricePerUnit','ssp.totalPrice','ssp.updateDate','p.productName','mjc.majorCateName','mnc.minorCateName','minc.miniCateName');
		$sLimit = "";

	
		if ( isset( $_POST['iSortCol_0'] ) ){
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ ){
				if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" ){
					$sOrder .= $aColumns[ intval( $_POST['iSortCol_'.$i] ) ]."
						".$_POST['sSortDir_'.$i].", ";
						//".pg_escape_string( $_POST['sSortDir_'.$i] ) .", ";
				}
			}
			  
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" ){
				$sOrder = "";
			}
		}
	
		if ( $_POST['sSearch'] != "" ){
			$sWhere .= " AND (";
			for ( $i=0 ; $i<count($aColumns); $i++ ){
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch']."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		
		for ( $i=0 ; $i<count($aColumns)-4; $i++ ){
			if ( $_POST['bSearchable_'.$i] == "true" && $_POST['sSearch_'.$i] != '' ){
				if ( $sWhere == "" ){
					$sWhere = "WHERE ";
				}else{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch_'.$i]."%'";
			}
		}
		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1'){
			$sLimit = "LIMIT ".$_POST['iDisplayStart'].", ".$_POST['iDisplayLength'];
			$totalCountQuery = "SELECT count(distinct(siteProductId)) as total
				FROM $sTable
				$sWhere";
				
		}

		if ($_POST['iSortCol_0'] == 0){
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				ORDER BY ssp.updateDate desc 
				$sLimit";
		}else{
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				$sOrder
				$sLimit";
		}

		
		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' ){
			$resultdata = $this->db->query($sQuery);
			$resultdata =$resultdata->result();
			$totalData = $this->db->query($totalCountQuery);
			$totalData =$totalData->result();
			$result = array('squery'=>$resultdata,'total'=>$totalData);
		}else{
			$result = $this->db->query($sQuery);
			$result =$result->result();
		}

		$rResult = $result['squery'];
		$rTotal = $result['total'][0]->total;
		$output = array(
				"sEcho" =>$_POST['sEcho'],
				"iTotalRecords" => $rTotal,       
				"iTotalDisplayRecords" => $rTotal,
				"aaData" => array()
				);
		
		$countRow = $_POST['iDisplayStart'] + 1;
		$counter = 0;
		$resultRow = array();

		foreach($rResult as $value){
			$row = array();
			$row[0] = $countRow;
			$row[1] = $value->productId;
			$row[2] = $value->productName;
			$row[3] = $value->majorCateName;
			$row[4] = $value->minorCateName;
			$row[5] = $value->miniCateName;
			
					
			$row[6] = $value->issuedQuantity.' '.ucfirst($value->measuringUnit);
			$row[7] = $value->totalPrice;
			
			$row[8] = date("M d, Y", strtotime($value->updateDate));
			
			$row[9] = '<button class="btn btn-primary" id="freezeProduct_'.$value->productId.'" title="Assign Product" style="padding:3px 10px; margin-right:0px;" onclick="addProductInTable('.$value->productId.',\''.$value->productName.'\','.$value->issuedQuantity.',\''.ucfirst($value->measuringUnit).'\','.$value->pricePerUnit.')">Add <i class="fa fa-arrow-circle-o-down"></i></button>';
			
			
			$countRow += 1;
			$resultRow[] =$row;
		}


		$output['aaData'] = $resultRow;
		echo json_encode($output);
	}

	

	function saveProductConsumption(){

		if(count($_POST['pricePerUnit'])==count($_POST['productId'])){
			$date = date('Y-m-d H:i:s');
			
			$text= 'Product Added successfully with Id ';
			$text2= 'Product Not Added successfully with Id ';
			for($i=0;$i<count($_POST['productId']);$i++){
				$data =array(
					'productId'			=>$_POST['productId'][$i]['value'],
					'consumedQuantity'	=>$_POST['productQuantity'][$i]['value'],
					'pricePerUnit'		=>$_POST['pricePerUnit'][$i]['value'],
					'totalPrice' 		=>$_POST['pricePerUnit'][$i]['value']*$_POST['productQuantity'][$i]['value'],
					'updateDate'		=>$date
					);
				die;
				$query = $this->db->insert('consumed_products', $data);
				if($query==true){
					$success = TRUE;
					$text .= $_POST['productId'][$i]['value'].', ' ;
				
				}else{
					$success = FALSE;
					$text2 .= $_POST['productId'][$i]['value'].', ' ;
				}
			}
			if($success==true){
				echo json_encode(array('success'=>$success,'text'=>rtrim($text,', ')));
			}else{
				echo json_encode(array('success'=>$success,'text'=>rtrim($text2,', ')));
			}
				


		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Server not respond!, Please try later!'));
		}

	}

	
}