<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_product_model extends CI_Model {
	public function __construct(){
		$this->load->database();
	}

	function getActiveProducts(){
		$sIndexColumn = "productId";
		$sTable = "products p";
		
			$sWhere ="right join users u on u.userId = p.addedBy
						left join major_category mjc on mjc.majorCateId = p.majorCateId
						left join minor_category mnc on mnc.minorCateId = p.minorCateId
						left join mini_category minc on minc.miniCateId = p.miniCateId 
						where p.delete_flag=1 and u.privilege in (51,99)";
		$aColumns = array( 'productId','addedBy','u.firstName','productName','productShortName','majorCateName','minorCateName','miniCateName','totalPrice', 'p.status','p.insertDate','quantity','measuringUnit','pricePerUnit','productQuantityLeft(p.productId) issuedQuantity','(p.quantity-productQuantityLeft(p.productId)) leftQuantity');
		$sLimit = "";

	
		if ( isset( $_POST['iSortCol_0'] ) ){
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ ){
				if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" ){
					$sOrder .= $aColumns[ intval( $_POST['iSortCol_'.$i] ) ]."
						".$_POST['sSortDir_'.$i].", ";
						//".pg_escape_string( $_POST['sSortDir_'.$i] ) .", ";
				}
			}
			  
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" ){
				$sOrder = "";
			}
		}
	
		if ( $_POST['sSearch'] != "" ){
			$sWhere .= " AND (";
			for ( $i=0 ; $i<count($aColumns); $i++ ){
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch']."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		
		for ( $i=0 ; $i<count($aColumns)-4; $i++ ){
			if ( $_POST['bSearchable_'.$i] == "true" && $_POST['sSearch_'.$i] != '' ){
				if ( $sWhere == "" ){
					$sWhere = "WHERE ";
				}else{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch_'.$i]."%'";
			}
		}
		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1'){
			$sLimit = "LIMIT ".$_POST['iDisplayStart'].", ".$_POST['iDisplayLength'];
			$totalCountQuery = "SELECT count(distinct(productId)) as total
				FROM $sTable
				$sWhere";
				
		}

		if ($_POST['iSortCol_0'] == 0){
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				ORDER BY p.insertDate desc 
				$sLimit";
		}else{
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				$sOrder
				$sLimit";
		}

		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' ){
			$resultdata = $this->db->query($sQuery);
			$resultdata =$resultdata->result();
			$totalData = $this->db->query($totalCountQuery);
			$totalData =$totalData->result();
			$result = array('squery'=>$resultdata,'total'=>$totalData);
		}else{
			$result = $this->db->query($sQuery);
			$result =$result->result();
		}

		$rResult = $result['squery'];
		$rTotal = $result['total'][0]->total;
		$output = array(
				"sEcho" =>$_POST['sEcho'],
				"iTotalRecords" => $rTotal,       
				"iTotalDisplayRecords" => $rTotal,
				"aaData" => array()
				);
		
		$countRow = $_POST['iDisplayStart'] + 1;
		$counter = 0;
		$resultRow = array();

		foreach($rResult as $value){
			$row = array();
			$row[0] = $countRow;
			$row[1] = $value->productId;
			$row[2] = $value->productName;
			$row[3] = $value->majorCateName;
			$row[4] = $value->minorCateName;
			$row[5] = $value->miniCateName;
			if($value->status=='Active'){
				$row[6] = '<button class="btn btn-success" title="Change Status ?" style="padding:3px 10px; margin-right:0px;" onclick="switchProduct(\'InActive\','.$value->productId.')">'.$value->status.'</button>';
			}else{
				$row[6] = '<button class="btn btn-warning" title="Change Status ?" style="padding:3px 10px; margin-right:0px;" onclick="switchProduct(\'Active\','.$value->productId.')">'.$value->status.'</button>';
			}
			
					
			$row[7] = $value->leftQuantity.' '.ucfirst($value->measuringUnit);
			$row[8] = $value->totalPrice;
			
			$row[9] = date("M d, Y", strtotime($value->insertDate));
			$row[10] = $value->firstName;
			if($value->status=='Active'){
				$row[11] = '<button class="btn btn-primary" id="freezeProduct_'.$value->productId.'" title="Assign Product" style="padding:3px 10px; margin-right:0px;" onclick="addProductInTable('.$value->productId.',\''.$value->productName.'\','.$value->leftQuantity.',\''.ucfirst($value->measuringUnit).'\','.$value->pricePerUnit.')">Add <i class="fa fa-arrow-circle-o-down"></i></button>';
			}else{
				$row[11] = '<button class="btn btn-danger" title="This prodcut not to be assigned" style="padding:3px 10px; margin-right:0px;">Not <i class="fa fa-warning"></i></button>';
			}
			
			$countRow += 1;
			$resultRow[] =$row;
		}


		$output['aaData'] = $resultRow;
		echo json_encode($output);
	}

	function getSiteInChargeList(){
		$query = $this->db->query("SELECT userId,firstName,lastName FROM users WHERE delete_flag=1 AND status='Active' AND privilege = 1");
		if($query->num_rows()>0){
			return $result = $query->result();
			//return $result;
		}else{
			
		}
	}

	function saveSiteInChargeProducts(){

		if(count($_POST['pricePerUnit'])==count($_POST['productId']) && !empty($_POST['siteInChargeId'])){
			$date = date('Y-m-d H:i:s');
			
			$text= 'Product Added successfully with Id ';
			$text2= 'Product Not Added successfully with Id ';
			for($i=0;$i<count($_POST['productId']);$i++){
				$data =array(
					'siteInChargeId'	=>$_POST['siteInChargeId'],
					'productId'			=>$_POST['productId'][$i]['value'],
					'issuedQuantity'	=>$_POST['productQuantity'][$i]['value'],
					'pricePerUnit'		=>$_POST['pricePerUnit'][$i]['value'],
					'totalPrice' 		=>$_POST['pricePerUnit'][$i]['value']*$_POST['productQuantity'][$i]['value'],
					'updateDate'		=>$date
					);
				$query = $this->db->insert('site_products', $data);
				if($query==true){
					$success = TRUE;
					$text .= $_POST['productId'][$i]['value'].', ' ;
				
				}else{
					$success = FALSE;
					$text2 .= $_POST['productId'][$i]['value'].', ' ;
				}
			}
			if($success==true){
				echo json_encode(array('success'=>$success,'text'=>rtrim($text,', ')));
			}else{
				echo json_encode(array('success'=>$success,'text'=>rtrim($text2,', ')));
			}
				


		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Server not respond!, Please try later!'));
		}

	}

	
}