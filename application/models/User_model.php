<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	public function __construct(){
		$this->load->database();
	}

	function do_login(){
		$this->load->library('session');		
		if($this->session->userdata('logged_in')==1){
			return false;	
		}else{
			$remember_me= $this->input->post('remember');
			$email = $this->input->post('username');
			$passowrd = $this->input->post('password');
			
			$query_email = $this->db->query("select * from users where emailId='" .$email. "'");
			if($query_email->num_rows()==1){
				$query_email_status = $this->db->query("select * from users where emailId='" .$email. "' and status='InActive'");
				if($query_email_status->num_rows()==1){
					return json_encode(array('success'=>FALSE,'text'=>'Please check your mail account & confim your registration first!'));
				}else{
					$query_password = $this->db->query("select * from users where emailId='" .$email. "' and password='" .$passowrd. "'");
					if($query_password->num_rows()==1){
						
						$query = $this->db->query("select * from users where emailId='" .$email. "' and password = '".$passowrd."' and status='Active'");
						if($query->num_rows()==1){	
							$data = $query->result();
							
							$query2 = $this->db->query("select loginTime,loginIPAddress  from user_logs where userId=".$data[0]->userId." order by loginTime desc limit 1 ");
							$lastLoginTime='';
							$lastIPAddress='';
							if($query2->num_rows()==1){	
								$data2 = $query2->result();
								$lastLoginTime = $data2[0]->loginTime;
								$lastIPAddress = $data2[0]->loginIPAddress;						
							}

							$StarData = array(
								'login_network'=> 'RMS Construction',
								'email' => $data[0]->emailId,
								'userId' => $data[0]->userId,
								'fullname' => ucfirst($data[0]->firstName).' '.$data[0]->middleName.' '.$data[0]->lastName,
								'firstname'=> $data[0]->firstName,
								'middlename'=> $data[0]->middleName,
								'lastname' => $data[0]->lastName,
								'profile' =>$data[0]->profile,
								'privilege' => $data[0]->privilege, //userType must be 1
								'lastLoginTime'=>$lastLoginTime,
								'lastIPAddress'=>$lastIPAddress,
								'logged_in' => TRUE
							);
							$this->session->set_userdata($StarData);
							return json_encode(array('success'=>TRUE,'text'=>'Login successfully!'));
						}else{
							return json_encode(array('success'=>FALSE,'text'=>'Login failed!'));
						}
							
					}else{
						return json_encode(array('success'=>FALSE,'text'=>'You entered wrong email or passowrd '));
					}
				}
				
			}else{
				return json_encode(array('success'=>FALSE,'text'=>'This Email address is not registered!'));
			}
		}
	}
	
	function getUser($userId){
		$query = $this->db->query("select userId,emailId,mobile,officeLandLine,status,privilege,userToken,updateDate,firstName,middleName,lastName,gender,profile,address1,address2,city,state,country,pincode,dob from users where userId='" .$userId. "'");
		if($query->num_rows()==1){	
			$row = $query->result();
			echo json_encode(array('success'=>TRUE,'data'=>$row[0]));
		}
	}

	function getUserProfile($userId){
		$query = $this->db->query("select userId,emailId,mobile,officeLandLine,status,privilege,userToken,updateDate,firstName,middleName,lastName,gender,profile,address1,address2,city,state,country,pincode,dob from users where userId='" .$userId. "'");
		if($query->num_rows()==1){	
			$row = $query->result();
			return $row[0];
		}
	}

	function updateUser($userId){
		$userId = $this->input->post('userId');
		$userToken = $this->input->post('userToken');
		$firstname = $this->input->post('firstname');
		$middlename = $this->input->post('middlename');
		$lastname = $this->input->post('lastname');
		$profile = $this->input->post('profile');
		$gender = $this->input->post('gender');
		$privilege = $this->input->post('userRole');
		$dob = $this->input->post('dob');
		$mobile = $this->input->post('mobile');
		$email = $this->input->post('email');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$country = $this->input->post('country');
		$address1 = addslashes($this->input->post('address1'));
		$address2 = addslashes($this->input->post('address2'));
		$pincode = $this->input->post('pincode');
		$landline = $this->input->post('landline');

		$randomString = $userToken;
		$status = "InActive";
		$name = ucfirst($firstname).' '.$lastname;

		$date = date('Y-m-d H:i:s');
		$query0 = $this->db->query("select password from users where userId='".$userId."' and userToken = '".$userToken."'");
		
		if($query0->num_rows()==1){

			$row = $query0->result();
			$password = $row[0]->password;

			$data = array(
				'emailId'		=> $email,
				'mobile'		=> $mobile,
				'officeLandLine'=> $landline,
				'status'		=> $status,
				'privilege'		=> $privilege,
				'dob'			=> $dob,
				'firstName' 	=> $firstname,
				'middleName'	=> $middlename,
				'lastName' 		=> $lastname,
				'gender'		=> $gender,
				'profile'		=> $profile,
				'address1'		=> $address1,
				'address2'		=> $address2,
				'city'			=> $city,
				'state'			=> $state,
				'country'		=> $country,
				'pincode'		=> $pincode,
				'updateDate' 	=> $date
				);
	
			$query = $this->db->update('users', $data,'userId='.$userId);
			
			if($query =="true"){
				$url = base_url().'authenticate/token/'.$randomString;
				echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the administrator & given email address, kindly check and confirm ','mail'=>$this->sendMailUpdatedUser($url,$name,$password,$email)));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Sorry Some problem occure !'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'The given email address already registered, Please check your mail inbox.'));
		}
	}

	

	function registration(){
		$firstname = $this->input->post('firstname');
		$middlename = $this->input->post('middlename');
		$lastname = $this->input->post('lastname');
		$profile = $this->input->post('profile');
		$gender = $this->input->post('gender');
		$privilege = $this->input->post('userRole');
		$dob = $this->input->post('dob');
		$mobile = $this->input->post('mobile');
		$email = $this->input->post('email');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$country = $this->input->post('country');
		$address1 = addslashes($this->input->post('address1'));
		$address2 = addslashes($this->input->post('address2'));
		$pincode = $this->input->post('pincode');
		$landline = $this->input->post('landline');
		
		$randomString = md5($email.time()); //For sending Email 
		$password = $this->generate_password();
		
		$status = "InActive";
		$name = ucfirst($firstname).' '.$lastname;
		$date = date('Y-m-d H:i:s');
		$query0 = $this->db->query("select emailId from users where emailId='".$email."'");
		
		if($query0->num_rows()==0){
			$data = array(
				'emailId'		=> $email,
				'password'		=> $password,
				'mobile'		=> $mobile,
				'officeLandLine'=> $landline,
				'status'		=> $status,
				'privilege'		=> $privilege,
				'dob'			=> $dob,
				'firstName' 	=> $firstname,
				'middleName'	=> $middlename,
				'lastName' 		=> $lastname,
				'gender'		=> $gender,
				'profile'		=> $profile,
				'address1'		=> $address1,
				'address2'		=> $address2,
				'city'			=> $city,
				'state'			=> $state,
				'country'		=> $country,
				'pincode'		=> $pincode,
				'updateDate' 	=> $date,
				'userToken' 	=> $randomString
				);
	
			$query = $this->db->insert('users', $data);
			
			if($query =="true"){
				$url = base_url().'authenticate/token/'.$randomString;
				echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the administrator & given email address, kindly check and confirm ','mail'=>$this->sendMailRegistration($url,$name,$password,$email)));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Sorry Some problem occure !'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'The given email address already registered, Please check your mail inbox.'));
		}
	}

	function sendMailRegistration($url,$name,$password,$email){
		$Body = file_get_contents('emailTemplate/F_registration.html');
		$tochange = array("[URL]", "[NAME]","[PASSWORD]");
		$changed   = array($url,$name,$password);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@rmsconstruction.in','RMS CONSTRUCTION'); // change it to yours
		$this->email->to($email);// change it to yours
		//$this->email->cc('admin@rmsconstruction.in'); 
		$this->email->cc('surendra.vimal27@gmail.com'); 
		$this->email->subject('Confirm your registration at RMS Construction');
		$this->email->message($message);
		if($this->email->send()){
			return 'Mail sent successfully';
		}else{
			return 'mail not sent';
		}
	}

	function sendMailUpdatedUser($url,$name,$password,$email){
		$Body = file_get_contents('emailTemplate/F_registration.html');
		$tochange = array("[URL]", "[NAME]","[PASSWORD]");
		$changed   = array($url,$name,$password);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@rmsconstruction.in','RMS CONSTRUCTION'); // change it to yours
		$this->email->to($email);// change it to yours
		//$this->email->cc('admin@rmsconstruction.in'); 
		$this->email->cc('surendra.vimal27@gmail.com'); 
		$this->email->subject('Confirm your updated details at RMS Construction');
		$this->email->message($message);
		if($this->email->send()){
			return 'Mail sent successfully';
		}else{
			return 'mail not sent';
		}
	}


	function authenticate($token){
		$query = $this->db->query("select emailId from users where userToken='" .$token. "'");
		if($query->num_rows()==1){	
			$row = $query->result();
			$emailId = $row[0]->emailId;
			$query1 = $this->db->query("update users set status='Active' where emailId='".$emailId."'");
			
			if($query1=='true'){
				return 'True';
			}else{
				return 'False';	
			}
		}
	}
	
	function getUserListing(){
		$sIndexColumn = "userId";
		$sTable = "users";
		if($this->session->userdata('privilege')==99){
			$sWhere ="where delete_flag=1 ";
		}else{
			$sWhere ="where privilege NOT IN (99,51) and delete_flag=1 ";
		}
		
		$aColumns = array( 'userId','firstName','middleName','lastName','emailId','mobile','city', 'status','insertDate','privilege');
		$sLimit = "";
		
		if ( isset( $_POST['iSortCol_0'] ) ){
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ ){
				if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" ){
					$sOrder .= $aColumns[ intval( $_POST['iSortCol_'.$i] ) ]."
						".$_POST['sSortDir_'.$i].", ";
						//".pg_escape_string( $_POST['sSortDir_'.$i] ) .", ";
				}
			}
			  
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" ){
				$sOrder = "";
			}
		}
	
		if ( $_POST['sSearch'] != "" ){
			$sWhere .= " AND (";
			for ( $i=0 ; $i<count($aColumns)-1 ; $i++ ){
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch']."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns); $i++ ){
			if ( $_POST['bSearchable_'.$i] == "true" && $_POST['sSearch_'.$i] != '' ){
				if ( $sWhere == "" ){
					$sWhere = "WHERE ";
				}else{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch_'.$i]."%'";
			}
		}
		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1'){
			$sLimit = "LIMIT ".$_POST['iDisplayStart'].", ".$_POST['iDisplayLength'];
			$totalCountQuery = "SELECT count(distinct(userId)) as total
				FROM $sTable
				$sWhere";
				
		}

		if ($_POST['iSortCol_0'] == 0){
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				GROUP BY emailId
				ORDER BY insertDate desc 
				$sLimit";
		}else{
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				$sOrder
				$sLimit";
		}

		
		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' ){
			$resultdata = $this->db->query($sQuery);
			$resultdata =$resultdata->result();
			$totalData = $this->db->query($totalCountQuery);
			$totalData =$totalData->result();
			$result = array('squery'=>$resultdata,'total'=>$totalData);
		}else{
			$result = $this->db->query($sQuery);
			$result =$result->result();
		}

		$rResult = $result['squery'];
		$rTotal = $result['total'][0]->total;
		$output = array(
				"sEcho" =>$_POST['sEcho'],
				"iTotalRecords" => $rTotal,       
				"iTotalDisplayRecords" => $rTotal,
				"aaData" => array()
				);
		
		$countRow = $_POST['iDisplayStart'] + 1;
		$counter = 0;
		$resultRow = array();

		foreach($rResult as $value){
			$row = array();
			$row[0] = $countRow;
			$row[1] = $value->userId;
			$row[2] = ucfirst($value->firstName).' '.$value->lastName;
			$row[3] = $value->emailId;
			$row[4] = $value->mobile;
			if($value->privilege==99){
				$row[5] = 'Supar Admin';
			}else if($value->privilege==51){
				$row[5] = 'Sub Admin';
			}else{
				$row[5] = 'Site InCharge';
			}
			if($value->status=='Active'){
				$row[6] = '<span class="label label-sm label-success">'.$value->status.'</span>';
			}else{
				$row[6] = '<span class="label label-sm label-warning">'.$value->status.'</span>';
			}
				
			$row[7] = $value->city;
			$row[8] = date("M d, Y", strtotime($value->insertDate));
			$row[9] = '<a href="javascript:void(0);" title="edit user" onClick="updateUser('.$value->userId.',\'get\')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" title="remove user" onclick="removeUser(\' '.ucfirst($value->firstName).' '.$value->lastName.' \','.$value->userId.')"><i class="fa fa-trash"></i></a>';
			$countRow += 1;
			$resultRow[] =$row;
		}


		$output['aaData'] = $resultRow;
		echo json_encode($output);
	}


	function removeUser($userId){
		$data = array( 'delete_flag' => 0);
		$query2 = $this->db->update('users', $data,'userId='.$userId);
		if($query2==true){
			echo json_encode(array('success'=>TRUE,'text'=>'User remove successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'User not remove, Please try after sometime !!'));
		}
	}


	function generate_password( $length = 6 ) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}

	function resetPassword(){
		$userId  = $this->session->userdata('userId');
		$oldPassword = $this->input->post('oldPassword');
		$newPassword = $this->input->post('newPassword');
		$confirmPassword = $this->input->post('confirmPassword');
		
		$query = $this->db->query("select * from users where userId='" .$userId. "' and password = '".$oldPassword."'");
		if($query->num_rows()==1){	
			if(strlen($newPassword)>5 || strlen($newPassword)<25){
				if(!empty($newPassword) && !empty($confirmPassword) && $newPassword==$confirmPassword){
					$data = array( 'password' => $newPassword);
					$query2 = $this->db->update('users', $data,'userId='.$userId);
					if($query2==true){
						echo json_encode(array('success'=>TRUE,'text'=>'Password reset successfully'));
					}else{
						echo json_encode(array('success'=>FALSE,'text'=>'Password not reset, Due to some error'));
					}
				}else{
					echo json_encode(array('success'=>FALSE,'text'=>'Your confirm password not matched'));
				}
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Password length should be greater than 5 and less than 25 characters'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'You have enter wrong old password'));
		}	
	}
	
	function updateUserProfile(){
		$userId = $this->session->userdata('userId');

		$firstname = $this->input->post('firstname');
		$middlename = $this->input->post('middlename');
		$lastname = $this->input->post('lastname');
		//$profile = $this->input->post('profile');
		$gender = $this->input->post('gender');
		//$privilege = $this->input->post('userRole');
		$dob = $this->input->post('dob');
		$mobile = $this->input->post('mobile');
		//$email = $this->input->post('email');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$country = $this->input->post('country');
		$address1 = addslashes($this->input->post('address1'));
		$address2 = addslashes($this->input->post('address2'));
		$pincode = $this->input->post('pincode');
		$landline = $this->input->post('landline');

		
		$date = date('Y-m-d H:i:s');
		
		
		$data = array(
			'mobile'		=> $mobile,
			'officeLandLine'=> $landline,
			'dob'			=> $dob,
			'firstName' 	=> $firstname,
			'middleName'	=> $middlename,
			'lastName' 		=> $lastname,
			'gender'		=> $gender,
			'address1'		=> $address1,
			'address2'		=> $address2,
			'city'			=> $city,
			'state'			=> $state,
			'country'		=> $country,
			'pincode'		=> $pincode,
			'updateDate' 	=> $date
			);


		
		$query = $this->db->update('users', $data,'userId='.$userId);
		
		if($query =="true"){
			echo json_encode(array('success'=>TRUE,'text'=>'Profile updated successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Sorry Some problem occure !'));
		}
		
	}


	function forget_password(){
		$email = $this->input->post('email');
		$query = $this->db->query("select emailId,password,firstName from users where emailId='" .$email. "' and status='Active' and delete_flag=1");
		
		if($query->num_rows()==1){	
			$row = $query->result();
			$password = $row[0]->password;
			$name = $row[0]->firstName;
			$url = base_url().'login';
			echo json_encode(array('success'=>TRUE,'text'=>'we retrieve your password successfully, kindly check your mail inbox ','mail'=>$this->sendMailForgetPassword($url,$name,$password,$email)));
			
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'This is not valid Email Address'));
		}
		
	}
	
	function sendMailForgetPassword($url,$name,$password,$email){
		$Body = file_get_contents('emailTemplate/F_forgetPassword.html');
		$tochange = array("[EMAIL]","[URL]", "[NAME]","[PASSWORD]");
		$changed   = array($email,$url,$name,$password);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@rmsconstruction.in','RMS CONSTRUCTION');  // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('surendra.vimal27@gmail.com,niwas_bhontele@yahoo.com'); 
		$this->email->subject('Recover forget password at RMS Construction');
		$this->email->message($message);
		if($this->email->send()){
			return 'Mail sent successfully';
		}else{
			return 'mail not sent';
		}
	}

	
	
}