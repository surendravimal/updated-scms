<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form_model extends CI_Model {
	
	public function __construct(){
        parent::__construct();
		$this->load->database();
		$this->load->library('encrypt');
		$this->load->helper('myencryption');	
    }
	
	public function index(){
	}
	
	function registration(){
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$country = $this->input->post('country');
		$city = $this->input->post('city');
		$address = addslashes($this->input->post('address'));
		$description = $this->input->post('description');
		$randomString = md5($email.time());
		
		$name = ucfirst($firstname).' '.$lastname;
		$date = date('Y-m-d H:i:s');
		$query0 = $this->db->query("select * from authors where email='".$email."'");
		
		if($query0->num_rows()==0){
			$data = array(
				'firstname' => $firstname,
				'lastname' => $lastname,
				'email' => $email,
				'phone' => $phone,
				'password'=> $password,
				'country'=> $country,
				'city'=> $city,
				'address'=> $address,
				'updateDate' => $date,
				'description' => $description,
				'token' => $randomString
				
				);
	
			$query = $this->db->insert('authors', $data);
			
			if($query =="true"){
				$url = base_url().'authenticate/token/'.$randomString;
				echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the email address, kindly check and confirm ','mail'=>$this->sendMailRegistration($url,$name,$password,$email)));
				//echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the email address, kindly check and confirm '));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Sorry Some problem occure !'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Email already registered & please login &nbsp;<a href="'.base_url().'login">Click Here</a>'));
		}
		
		
	
	}
	
	function sendMailRegistration($url,$name,$password,$email){
		$Body = file_get_contents('template/F_registration.html');
		$tochange = array("[URL]", "[NAME]","[PASSWORD]");
		$changed   = array($url,$name,$password);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@ijpot.org','IJPOT'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('no-reply@ijpot.org'); 
		$this->email->subject('Confirm your registration at IJPOT');
		$this->email->message($message);
		if($this->email->send()){
			return 'Mail sent successfully';
		}else{
			return 'mail not sent';
		}
	}
	
	
	function forget_password(){
		$email = $this->input->post('email');
		$query = $this->db->query("select firstname, password from authors where email='" .$email. "'");
		
		if($query->num_rows()==1){	
			$row = $query->result();
			$password = $row[0]->password;
			$name = $row[0]->firstname;
			$url = base_url().'login';
			echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the email address, kindly check and confirm ','mail'=>$this->sendMailPasswordReset($url,$name,$password,$email)));
			
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'this is not valid Email Address'));
		}
		
	}
	
	function sendMailPasswordReset($url,$name,$password,$email){
		$Body = file_get_contents('template/F_resetPassword.html');
		$tochange = array("[URL]", "[NAME]","[PASSWORD]");
		$changed   = array($url,$name,$password);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@ijpot.org','IJPOT'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('no-reply@ijpot.org'); 
		$this->email->subject('Recovering of forget password at IJPOT');
		$this->email->message($message);
		if($this->email->send()){
			return 'Mail sent successfully';
		}else{
			return 'mail not sent';
		}
	}
	
	function contactus(){
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$query = $this->input->post('query');
		
		$data = array(
            'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'query' => $query
		    );

		$query = $this->db->insert('contactus', $data);
		
		if($query =="true"){
			echo json_encode(array('success'=>TRUE,'text'=>'Thank you for contact us','mail'=>$this->sendMailContactus($email,$phone,$name)));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Sorry Some problem occure !!'));
		}
	
		
		//return json_encode(array('success'=>$return,'text'=>$text));
	}
	
	function sendMailContactus($email,$phone,$name){
		$Body = file_get_contents('template/F_query.html');
		$tochange = array("[NAME]", "[PHONE]", "[EMAIL]");
		$changed   = array($name, $phone, $email);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@ijpot.org','IJPOT'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('no-reply@ijpot.org'); 
		$this->email->subject('Thank You For Contact Us');
		$this->email->message($message);
		if($this->email->send()){
		return 'Mail sent successfully';
		}else{
		return 'mail not sent';
		}
	}
	
	function upload_paper(){
		$query10 = $this->db->query("select volume,issue from time_frame where startTime <= now() and endTime >= now()");
		$data10 = $query10->result();
		$volume = $data10[0]->volume;
		$issue = $data10[0]->issue;
		
		$query0 = $this->db->query("select journalId, volumeNum,IssueNum from journals  order by journalId desc limit 1");
		$data0 = $query0->result();
		$journalId=  $data0[0]->journalId+1;
		
		$authors ='';
		if(isset($_POST['author_1'])){$author1 = $_POST['author_1']; $authors .=$author1;}
		if(isset($_POST['author_2'])){$author2 = $_POST['author_2']; $authors .='~@~'.$author2;}
		if(isset($_POST['author_3'])){$author3 = $_POST['author_3']; $authors .='~@~'.$author3;}
		
		if(isset($_POST['reviewer_1'])){$reviewer1 = $_POST['reviewer_1'];}else{$reviewer1='';}
		if(isset($_POST['reviewer_2'])){$reviewer2 = $_POST['reviewer_2'];}else{$reviewer2='';}
		if(isset($_POST['reviewer_3'])){$reviewer3 = $_POST['reviewer_3'];}else{$reviewer3='';}
		
		if(isset($_POST['remail_1'])){$remail1 = $_POST['remail_1'];}else{$remail1='';}
		if(isset($_POST['remail_2'])){$remail2 = $_POST['remail_2'];}else{$remail2='';}
		if(isset($_POST['remail_3'])){$remail3 = $_POST['remail_3'];}else{$remail3='';}
		
		$filename = time().'-'.$volume.$issue.$journalId;
		$email = $this->session->userdata('email');
		$phone = $this->session->userdata('phone');
		$name = $this->session->userdata('fullname');
		
		$date = date('Y-m-d H:i:s');
		
		if(isset($_FILES['upload_file']) && $_FILES['upload_file']['name']!=''){
			$file= $_FILES['upload_file'];
			$return_data =uploadFile($file,$filename);
		}
		if(isset($return_data['error'])){
			echo json_encode(array('success'=>FALSE,'text'=>$return_data['error']['text']));
		}else{
			if(isset($return_data['result'])){
				$fileLink = 'papers/'.$return_data['result']['file_name'];
			}else{
				$fileLink='';
			}				
			$data=array(
				'volumeNum'=>$volume,
				'issueNum'=>$issue,
				'link' =>$fileLink,
				'paperId' =>$filename,
				'title' =>addslashes($this->input->post('title')),
				'abstractTxt'=>addslashes($_POST['abstract']),
				'authorId' =>$this->session->userdata('authorId'),
				'authorName'=>$authors,	
				'rvName1'=>$reviewer1,
				'rvName2'=>$reviewer2,
				'rvName3'=>$reviewer3,
				'rvEmail1'=>$remail1,
				'rvEmail2'=>$remail2,
				'rvEmail3'=>$remail3,						
				'updateDate'=>$date
				 );
			$query = $this->db->insert('journals', $data);
			if($query == true){
				echo json_encode(array('success'=>TRUE,'text'=>"Paper Uploaded successfully",'instruction'=>'Your paper id is <b>'.$filename.'</b> and check your mail for next updation','mail'=>$this->sendMailJournal($email,$phone,$name,$filename)));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>"Paper not uploaded"));
			}
		}
	}
	
	function sendMailJournal($email,$phone,$name,$filename){
		$Body = file_get_contents('template/F_paper_submission.html');
		$tochange = array("[NAME]", "[PHONE]", "[EMAIL]","[PAPERID]");
		$changed   = array($name, $phone, $email,$filename);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@ijpot.org','IJPOT'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('no-reply@ijpot.org'); 
		$this->email->subject('Your Paper Submitted Successfully');
		$this->email->message($message);
		if($this->email->send()){
		return 'Mail sent successfully';
		}else{
		return 'mail not sent';
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	


	
	
	
	
}