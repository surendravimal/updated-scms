<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model {
	public function __construct(){
		$this->load->database();
	}

	function getProductListing(){
		$sIndexColumn = "productId";
		$sTable = "products p";
		
			$sWhere ="right join users u on u.userId = p.addedBy
						left join major_category mjc on mjc.majorCateId = p.majorCateId
						left join minor_category mnc on mnc.minorCateId = p.minorCateId
						left join mini_category minc on minc.miniCateId = p.miniCateId 
						where p.delete_flag=1 ";
		
		

		$aColumns = array( 'productId','addedBy','u.firstName','productName','productShortName','majorCateName','minorCateName','miniCateName','totalPrice', 'p.status','p.insertDate','quantity','measuringUnit');
		$sLimit = "";
		
		if ( isset( $_POST['iSortCol_0'] ) ){
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ ){
				if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" ){
					$sOrder .= $aColumns[ intval( $_POST['iSortCol_'.$i] ) ]."
						".$_POST['sSortDir_'.$i].", ";
						//".pg_escape_string( $_POST['sSortDir_'.$i] ) .", ";
				}
			}
			  
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" ){
				$sOrder = "";
			}
		}
	
		if ( $_POST['sSearch'] != "" ){
			$sWhere .= " AND (";
			for ( $i=0 ; $i<count($aColumns)-1 ; $i++ ){
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch']."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns)-1; $i++ ){
			if ( $_POST['bSearchable_'.$i] == "true" && $_POST['sSearch_'.$i] != '' ){
				if ( $sWhere == "" ){
					$sWhere = "WHERE ";
				}else{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch_'.$i]."%'";
			}
		}
		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1'){
			$sLimit = "LIMIT ".$_POST['iDisplayStart'].", ".$_POST['iDisplayLength'];
			$totalCountQuery = "SELECT count(distinct(productId)) as total
				FROM $sTable
				$sWhere";
				
		}

		if ($_POST['iSortCol_0'] == 0){
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				ORDER BY p.insertDate desc 
				$sLimit";
		}else{
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
				FROM $sTable
				$sWhere
				$sOrder
				$sLimit";
		}

		
		
		if ( isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' ){
			$resultdata = $this->db->query($sQuery);
			$resultdata =$resultdata->result();
			$totalData = $this->db->query($totalCountQuery);
			$totalData =$totalData->result();
			$result = array('squery'=>$resultdata,'total'=>$totalData);
		}else{
			$result = $this->db->query($sQuery);
			$result =$result->result();
		}

		$rResult = $result['squery'];
		$rTotal = $result['total'][0]->total;
		$output = array(
				"sEcho" =>$_POST['sEcho'],
				"iTotalRecords" => $rTotal,       
				"iTotalDisplayRecords" => $rTotal,
				"aaData" => array()
				);
		
		$countRow = $_POST['iDisplayStart'] + 1;
		$counter = 0;
		$resultRow = array();

		foreach($rResult as $value){
			$row = array();
			$row[0] = $countRow;
			$row[1] = $value->productId;
			$row[2] = $value->productName;
			$row[3] = $value->majorCateName;
			$row[4] = $value->minorCateName;
			$row[5] = $value->miniCateName;
			if($this->session->userdata('privilege')==1 && $this->session->userdata('userId')==$value->addedBy){
				if($value->status=='Active'){
					$row[6] = '<span class="label label-sm label-success">'.$value->status.'</span>';
				}else{
					$row[6] = '<span class="label label-sm label-warning">'.$value->status.'</span>';
				}
			}else if($this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99){
				if($value->status=='Active'){
					$row[6] = '<span class="label label-sm label-success" title="Change Status ?" style="cursor:pointer" onclick="switchProduct(\'InActive\','.$value->productId.')">'.$value->status.'</span>';
				}else{
					$row[6] = '<span class="label label-sm label-warning" title="Change Status ?" style="cursor:pointer" onclick="switchProduct(\'Active\','.$value->productId.')">'.$value->status.'</span>';
				}
			}else{
				if($value->status=='Active'){
					$row[6] = '<span class="label label-sm label-success">'.$value->status.'</span>';
				}else{
					$row[6] = '<span class="label label-sm label-warning">'.$value->status.'</span>';
				}
			}
					
			$row[7] = $value->quantity.' '.ucfirst($value->measuringUnit);
			if($this->session->userdata('privilege')==1 && $this->session->userdata('userId')==$value->addedBy){
				$row[8] = $value->totalPrice;
			}else if($this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99 ){
				$row[8] = $value->totalPrice;
			}else{
				$row[8] = '-';
			}
			$row[9] = date("M d, Y", strtotime($value->insertDate));
			$row[10] = $value->firstName;
			if($this->session->userdata('privilege')==1 && $this->session->userdata('userId')==$value->addedBy){
				$row[11] = '<a href="javascript:void(0);" title="edit prodcut" onClick="updateProduct('.$value->productId.',\'get\')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" title="remove product" onclick="removeProduct(\' '.$value->productName.' \','.$value->productId.')"><i class="fa fa-trash"></i></a>';
			}else if($this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99){
				$row[11] = '<a href="javascript:void(0);" title="edit prodcut" onClick="updateProduct('.$value->productId.',\'get\')"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" title="remove product" onclick="removeProduct(\' '.$value->productName.' \','.$value->productId.')"><i class="fa fa-trash"></i></a>';
			}else{
				$row[11] = '<a href="javascript:void(0);" style="color:#ddd" title="prodcut not editable"><i class="fa fa-edit"></i></a>&nbsp;|&nbsp;<a href="javascript:void(0);" style="color:#ddd" title="product not removed"><i class="fa fa-trash"></i></a>';
			}
			
			$countRow += 1;
			$resultRow[] =$row;
		}


		$output['aaData'] = $resultRow;
		echo json_encode($output);
	}

	function getProduct($productId){
		$query = $this->db->query("select * from products where productId='" .$productId. "'");
		if($query->num_rows()==1){	
			$row = $query->result();
			echo json_encode(array('success'=>TRUE,'data'=>$row[0]));
		}
	}


	function removeProduct($productId){
		$data = array( 'delete_flag' => 0);
		$query2 = $this->db->update('products', $data,'productId='.$productId);
		if($query2==true){
			echo json_encode(array('success'=>TRUE,'text'=>'Product removed successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Product not removed, Please try after sometime !!'));
		}
	}

	function getMajorCate(){
		$query = $this->db->query("SELECT majorCateName,majorCateId,superCateShortName FROM major_category");
		if($query->num_rows()>0){
			$result = $query->result();
			return $result;
		}else{
			
		}
	}


	function getMinorCate($majorCateId){
		$query = $this->db->query("SELECT minorCateName,minorCateId FROM minor_category where majorCateId='".$majorCateId."'");
		if($query->num_rows()>0){
			$result = $query->result();
			return $result;
		}else{
			
		}
	}


	function getMinorCate2(){
		$query = $this->db->query("SELECT mjc.majorCateName,minorCateName,minorCateId FROM minor_category mnc inner join major_category mjc on mjc.majorCateId=mnc.majorCateId");
		if($query->num_rows()>0){
			$result = $query->result();
			return $result;
		}else{
			
		}
	}

	function getMiniCate($majorCateId,$minorCateId){
		$query = $this->db->query("SELECT miniCateName,miniCateId FROM mini_category WHERE minorCateId='".$minorCateId."' and majorCateId='".$majorCateId."'");

		if($query->num_rows()>0){
			$result = $query->result();
			return $result;
		}else{
			
		}
	}


	function getMiniCate2(){
		$query = $this->db->query("SELECT miniCateName,miniCateId,mnc.minorCateName,mjc.majorCateName FROM mini_category minc inner join minor_category mnc on mnc.minorCateId=minc.minorCateId inner join major_category mjc on mjc.majorCateId=minc.majorCateId");

		if($query->num_rows()>0){
			$result = $query->result();
			return $result;
		}else{
			
		}
	}

	function addMajorCategory(){
		$majorCateName = $this->input->post('majorCateName');
		$query = $this->db->query("SELECT * FROM major_category WHERE majorCateName = '".$majorCateName."'");
		
		if($query->num_rows()==0){
			$data = array('majorCateName'=>$majorCateName);
			$query1 =$this->db->insert('major_category',$data);
			if($query1==TRUE){
				echo json_encode(array('success'=>TRUE,'text'=>'Category added successfully!'));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Server not respond!, Please try later!'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Given category already existed!'));
		}
	}

	function addMinorCategory(){
		$majorCateId = $this->input->post('majorCateId');
		$minorCateName = $this->input->post('minorCateName');
		$query = $this->db->query("SELECT * FROM minor_category WHERE minorCateName = '".$minorCateName."' and majorCateId='".$majorCateId."'");
		
		if($query->num_rows()==0){
			$data = array('minorCateName'=>$minorCateName,'majorCateId'=>$majorCateId);
			$query1 =$this->db->insert('minor_category',$data);
			if($query1==TRUE){
				echo json_encode(array('success'=>TRUE,'text'=>'Category added successfully!'));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Server not respond!, Please try later!'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Given sub category already existed in main category!'));
		}	
	}

	function addMiniCategory(){
		$majorCateId = $this->input->post('majorCateId');
		$minorCateId = $this->input->post('minorCateId');
		$miniCateName = $this->input->post('miniCateName');

		$query = $this->db->query("SELECT * FROM mini_category WHERE miniCateName = '".$miniCateName."' and minorCateId='".$minorCateId."' and majorCateId='".$majorCateId."'");
		
		if($query->num_rows()==0){
			$data = array('miniCateName'=>$miniCateName,'majorCateId'=>$majorCateId,'minorCateId'=>$minorCateId);
			$query1 =$this->db->insert('mini_category',$data);
			if($query1==TRUE){
				echo json_encode(array('success'=>TRUE,'text'=>'Category added successfully!'));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Server not respond!, Please try later!'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Given category already existed!'));
		}	
	}

	function addNewProduct(){
		$date = date('Y-m-d H:i:s');
		if($this->session->userdata('privilege')==1){
			$status='InActive';
		}else{
			$status='Active';
		}
		$data = array(
			'majorCateId' 	=> $this->input->post('majorCategory'),
			'minorCateId' 	=> $this->input->post('minorCategory'),
			'miniCateId' 	=> $this->input->post('miniCategory'),

			'productShortName' => $this->input->post('productShortName'),
			'productName' => $this->input->post('productName'),
			'measuringUnit' => $this->input->post('measuringUnit'),

			'quantity' => $this->input->post('quantity'),
			'pricePerUnit' => $this->input->post('pricePerUnit'),
			'totalPrice' => $this->input->post('quantity')*$this->input->post('pricePerUnit'),
			'updateDate' =>$date,
			'productDescription'=>'Product Description !!',
			'status'=>$status,
			'addedBy'=>$this->session->userdata('userId')

		);

		
		$query = $this->db->insert('products',$data);
		if($query==true){
			echo json_encode(array('success'=>TRUE,'text'=>'Product Added successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Server not respond!, Please try later!'));
		}
	}


	function updateProduct($productId){
		$date = date('Y-m-d H:i:s');

		if($this->session->userdata('privilege')==1){
			$status='InActive';
		}else{
			$status='Active';
		}

		$data = array(
			'majorCateId' 	=> $this->input->post('majorCategory'),
			'minorCateId' 	=> $this->input->post('minorCategory'),
			'miniCateId' 	=> $this->input->post('miniCategory'),

			'productShortName' => $this->input->post('productShortName'),
			'productName' => $this->input->post('productName'),
			'measuringUnit' => $this->input->post('measuringUnit'),

			'quantity' => $this->input->post('quantity'),
			'pricePerUnit' => $this->input->post('pricePerUnit'),
			'totalPrice' => $this->input->post('quantity')*$this->input->post('pricePerUnit'),
			'updateDate' =>$date,
			'productDescription'=>'Product Description !!',
			'status'=>$status

		);

		
		$query = $this->db->update('products',$data,'productId='.$productId);
		if($query==true){
			echo json_encode(array('success'=>TRUE,'text'=>'Product Updated successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Server not respond!, Please try later!'));
		}
	}

	function changeStatus($status,$productId){
		$data = array( 'status' => $status);
		$query2 = $this->db->update('products', $data,'productId='.$productId);
		if($query2==true){
			echo json_encode(array('success'=>TRUE,'text'=>'Product status changed successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Product status not changed, Please try after sometime !!'));
		}
	}


	function deleteMajorCategory(){

		$this->db->where('majorCateId', $_POST['majorCateId']);
		$result = $this->db->delete('major_category');

		if($result==true){
			echo json_encode(array('success'=>TRUE,'text'=>'Category Deleted successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Cateogry Not Deleted , Please try after sometime !!'));
		}
	}

	function deleteMinorCategory(){

		$this->db->where('minorCateId', $_POST['minorCateId']);
		$result = $this->db->delete('minor_category');

		if($result==true){
			echo json_encode(array('success'=>TRUE,'text'=>'Category Deleted successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Cateogry Not Deleted , Please try after sometime !!'));
		}
	}

	function deleteMiniCategory(){

		$this->db->where('miniCateId', $_POST['miniCateId']);
		$result = $this->db->delete('mini_category');

		if($result==true){
			echo json_encode(array('success'=>TRUE,'text'=>'Category Deleted successfully'));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Cateogry Not Deleted , Please try after sometime !!'));
		}
	}
}