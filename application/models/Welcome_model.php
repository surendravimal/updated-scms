<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome_model extends CI_Model {
	
	public function __construct(){
        parent::__construct();
		$this->load->database();
		$this->load->library('encrypt');
		//$this->load->helper('myencryption');	
    }
	
	public function index(){
	}
	
	function update_logs(){
		$userId = $this->session->userdata('firstname');
		
		$loginIPAddres = $this->input->post('email');
		$loginBrowser = $this->input->post('phone');
		$loginDevice = $this->input->post('password');
		$loginLocation = $this->input->post('country');
		$loginPrivilege = $this->input->post('city');

	}


	function registration(){
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$country = $this->input->post('country');
		$city = $this->input->post('city');
		$address = addslashes($this->input->post('address'));
		$description = $this->input->post('description');
		$randomString = md5($email.time());
		
		$name = ucfirst($firstname).' '.$lastname;
		$date = date('Y-m-d H:i:s');
		$query0 = $this->db->query("select * from authors where email='".$email."'");
		
		if($query0->num_rows()==0){
			$data = array(
				'firstname' => $firstname,
				'lastname' => $lastname,
				'email' => $email,
				'phone' => $phone,
				'password'=> $password,
				'country'=> $country,
				'city'=> $city,
				'address'=> $address,
				'updateDate' => $date,
				'description' => $description,
				'token' => $randomString
				
				);
	
			$query = $this->db->insert('authors', $data);
			
			if($query =="true"){
				$url = base_url().'authenticate/token/'.$randomString;
				echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the email address, kindly check and confirm ','mail'=>$this->sendMailRegistration($url,$name,$password,$email)));
				//echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the email address, kindly check and confirm '));
			}else{
				echo json_encode(array('success'=>FALSE,'text'=>'Sorry Some problem occure !'));
			}
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Email already registered & please login &nbsp;<a href="'.base_url().'login">Click Here</a>'));
		}
	}
	
	function sendMailRegistration($url,$name,$password,$email){
		$Body = file_get_contents('template/F_registration.html');
		$tochange = array("[URL]", "[NAME]","[PASSWORD]");
		$changed   = array($url,$name,$password);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@ijpot.org','IJPOT'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('no-reply@ijpot.org'); 
		$this->email->subject('Confirm your registration at IJPOT');
		$this->email->message($message);
		if($this->email->send()){
			return 'Mail sent successfully';
		}else{
			return 'mail not sent';
		}
	}
	
	
	function forget_password(){
		$email = $this->input->post('email');
		$query = $this->db->query("select firstname, password from authors where email='" .$email. "'");
		
		if($query->num_rows()==1){	
			$row = $query->result();
			$password = $row[0]->password;
			$name = $row[0]->firstname;
			$url = base_url().'login';
			echo json_encode(array('success'=>TRUE,'text'=>'a confirmation mail has been sent to the email address, kindly check and confirm ','mail'=>$this->sendMailPasswordReset($url,$name,$password,$email)));
			
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'this is not valid Email Address'));
		}
	}
	
	function sendMailPasswordReset($url,$name,$password,$email){
		$Body = file_get_contents('template/F_resetPassword.html');
		$tochange = array("[URL]", "[NAME]","[PASSWORD]");
		$changed   = array($url,$name,$password);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@ijpot.org','IJPOT'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('no-reply@ijpot.org'); 
		$this->email->subject('Recovering of forget password at IJPOT');
		$this->email->message($message);
		if($this->email->send()){
			return 'Mail sent successfully';
		}else{
			return 'mail not sent';
		}
	}
	
	
	
	
	function contactus(){
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$query = $this->input->post('query');
		
		$data = array(
            'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'query' => $query
		    );

		$query = $this->db->insert('contactus', $data);
		
		if($query =="true"){
			echo json_encode(array('success'=>TRUE,'text'=>'Thank you for contact us','mail'=>$this->sendMailContactus($email,$phone,$name)));
		}else{
			echo json_encode(array('success'=>FALSE,'text'=>'Sorry Some problem occure !!'));
		}
	
		
		//return json_encode(array('success'=>$return,'text'=>$text));
	}
	
	function sendMailContactus($email,$phone,$name){
		$Body = file_get_contents('template/F_query.html');
		$tochange = array("[NAME]", "[PHONE]", "[EMAIL]");
		$changed   = array($name, $phone, $email);
		$message = str_replace($tochange, $changed , $Body);
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('no-reply@ijpot.org','IJPOT'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->cc('no-reply@ijpot.org'); 
		$this->email->subject('Thank You For Contact Us');
		$this->email->message($message);
		if($this->email->send()){
		return 'Mail sent successfully';
		}else{
		return 'mail not sent';
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	
	
	
	
}