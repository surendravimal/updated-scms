<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<title>Manage Category| RMS Construction !!</title>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="container">
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
      <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <?php echo left_menu('mcategory'); ?>
        <!-- END SIDEBAR MENU -->
      </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
              </div>
              <div class="modal-body">
                 Widget settings form goes here
              </div>
              <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        Manage Categories <small>category1,category2,category3</small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="<?php echo base_url(); ?>welcome">Home</a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>manage_categories">Manage Categories</a>
              <i class="fa fa-angle-right"></i>
            </li>
          </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">

          <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit"></i>Category 1
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  
                  <a href="javascript:;" class="reload">
                  </a>
                  
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-toolbar">
                  <div class="row">
                    

                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" placeholder="Add New Category1" name="addcate1" id="addcate1" class="form-control" />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="btn-group pull-right">
                        <button onclick="addMajorCategory();return false;" class="btn btn-primary">Add New</button>
                      </div>
                    </div>
                  </div>
                </div>
                <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                  <thead>
                    <tr>
                      <th>S.No.</th>
                      <th>Category Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="majorCategoryList">               
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        
          <div class="col-md-6">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit"></i>Category 2
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  
                  <a href="javascript:;" class="reload">
                  </a>
                  
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-toolbar">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <select class="form-control select2me majorCategory" name="majorCateIdForMinor" id="majorCateIdForMinor">
                            <option value="">Select..</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <input type="text" placeholder="Add New Category2" name="addcate2" id="addcate2" class="form-control" />
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="btn-group pull-right">
                        <button onclick="addMinorCategory();return false;" class="btn btn-primary">
                        Add New</i>
                        </button>
                      </div>
                    </div>
                    
                  </div>
                </div>
                <table class="table table-striped table-hover table-bordered" id="sample_editable_2">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Category2 Name</th>
                  <th>Category1 Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="majorCategoryList2">               
                </tbody>
                </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>
        <div class="row">
  
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit"></i>Category 3
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  
                  <a href="javascript:;" class="reload">
                  </a>
                  
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-toolbar">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <select class="form-control select2me" onchange="refreshMinorCate2(this.value); return false;" 
                        name="majorCateIdForMini" id="majorCateIdForMini">
                            <option value="">Select..</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <select class="form-control select2me minorCategory" name="minorCateIdForMini" id="minorCateIdForMini">
                            <option value="">Select..</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <input type="text" placeholder="Add New Category3" name="addcate3" id="addcate3" class="form-control" />
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="btn-group pull-right">
                        <button onclick="addMiniCategory();return false;" class="btn btn-primary">
                        Add New</i>
                        </button>
                      </div>
                    </div>
                    
                  </div>
                </div>
                <table class="table table-striped table-hover table-bordered" id="sample_editable_3">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Category3 Name</th>
                  <th>Category2 Name</th>
                  <th>Category1 Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="majorCategoryList3">               
                </tbody>
                </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>
        <!-- END PAGE CONTENT -->
      </div>
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <!--Cooming Soon...-->
    <!-- END QUICK SIDEBAR -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <div class="page-footer">
    <div class="page-footer-inner">
       2016 &copy; RMS Construction by pscreations.
    </div>
    <div class="scroll-to-top">
      <i class="icon-arrow-up"></i>
    </div>
  </div>
  <!-- END FOOTER -->
</div>


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-validation.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-samples.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/manage-categories.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-editable.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  Demo.init(); // init demo features
  FormValidation.init();
  FormSamples.init();
  
});
</script>



</body>
<!-- END BODY -->
