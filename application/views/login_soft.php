<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="<?php echo base_url(); ?>index.php">
	<!-- <img src="<?php echo base_url(); ?>assets/admin/layout2/img/logo-big-big.png" alt=""/> -->
	<span style="color: red; font-size: 2em;">RMS</span><span style="color: white; font-size: 2em;"> CONSTRUCTION</span>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<!-- onsubmit="do_login(); return false;" -->
	<form class="login-form" id="login_form" name="login_form" >
		<h3 class="form-title">Login to your account</h3>
		    <h4 class="form-title"> Welcome to RMS! You are just one step ahead to see your inventory. </h4>
	        <?php if(isset($success) && $success=='True'){?>
	          	<div class="alert alert-success display-show" style="text-transform:none;">
	          		<button class="close" data-close="alert"></button>
	          		Your registration is done Successfully
	          	</div>
	        <?php }?>
	   
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>Please enter username and password.</span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" easyui-validatebox="true" required="true" type="text" autocomplete="off" placeholder="Email Address" name="username"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
			</div>
		</div>
		
		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember" /> Remember me </label>
			<button type="submit" id="submit_button" class="btn blue pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		<div class="form-actions">
			<div class="" style="display:inline-flex;">
	          	<img id="loader" style="display: none; margin: 2px;" src="<?php echo base_url();?>assets/global/img/loader.gif" alt="wait.."/>
	            <span id="response" style="display:none; margin-top:15px; margin-left:10px;">Please wait..</span>
	          </div>
		</div>
		
		<div class="forget-password">
			<h4>Forgot your password ?</h4>
			<p>
				 no worries, click <a href="javascript:;" id="forget-password">
				here </a>
				to reset your password.
			</p>
		</div>
		
	</form>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form" name="forget-form" id="forget-form">
		<h3>Forget Password ?</h3>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="alert alert-danger display-hide2" style="display: none;">
			<button class="close" data-close="alert"></button>
			<span>Please enter username and password.</span>
		</div>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back </button>
			<button type="submit" id="forget-btn" class="btn blue pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		<div class="form-actions">
			<div class="" style="display:inline-flex;">
	          	<img id="loader2" style="display: none; margin: 2px;" src="<?php echo base_url();?>assets/global/img/loader.gif" alt="wait.."/>
	            <span id="response2" style="display:none; margin-top:15px; margin-left:10px;">Please wait..</span>
	          </div>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
	
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 <!-- 2016 &copy; Power By - Admin Dashboard Template. -->
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
 	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
  	Login.init();
  	Demo.init();
       // init background slide images
    $.backstretch([
        "<?php echo base_url(); ?>assets/admin/pages/media/bg/1.jpg",
        "<?php echo base_url(); ?>assets/admin/pages/media/bg/2.jpg",
        "<?php echo base_url(); ?>assets/admin/pages/media/bg/3.jpg",
        "<?php echo base_url(); ?>assets/admin/pages/media/bg/4.jpg"
        ], {
          fade: 1000,
          duration: 8000
    });

});
</script>

<script type="text/javascript">
	
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>