<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <body class="page-login" init-ripples="">
    <div class="center">
      <div class="card bordered z-depth-2" style="margin:0% auto; max-width:400px;">
        <div class="card-header">
          <div class="brand-logo">
            <div id="logo">
              <div class="foot1"></div>
              <div class="foot2"></div>
              <div class="foot3"></div>
              <div class="foot4"></div>
            </div> RMS Construction </div>
        </div>
        <div class="card-content">
          <div class="m-b-30">
            <div class="card-title strong pink-text">Login</div>
            <p class="card-title-desc"> Welcome to RMS! You are just one step ahead to see your inventory. </p>
            <?php if(isset($success) && $success=='True'){?>
              <p style="text-transform:none;">Your registration is done Successfully</p>
            <?php }?>
          </div>
          <form class="form-floating" id="login_form" name="login_form" onsubmit="do_login(); return false;">
            
            <div class="form-group">
              <label for="inputEmail" class="control-label">Email</label>
              <input type="text" class="form-control" name="inputEmail"> </div>
            <div class="form-group">
              <label for="inputPassword" class="control-label">Password</label>
              <input type="password" class="form-control" name="inputPassword"> </div>
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="rememberMe"> Remember me </label>
              </div>
            </div>
            <div class="card-action clearfix">
              <div class="pull-left" style="display:flex">
	          	<img id="loader" style="display: none; margin: 2px 8px;" src="<?php echo base_url();?>images/loader.gif" alt="wait.."/>
	            <span id="response" style="display:none; margin-top:8px; margin-left:10px;">Please wait..</span>
	          </div>
	          <div class="pull-right">
	            <button type="button" class="btn btn-link black-text">Forgot password</button>
	            <button type="submit" id="submit_button" class="btn btn-link black-text">Login</button>
	          </div>
	        </div>
          </form>
        </div>
      </div>
    </div>
    
    <script charset="utf-8" src="<?php echo base_url(); ?>assets/js/vendors.min.js"></script>
    <script charset="utf-8" src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
    <script type="text/javascript">
		function do_login(){
			$("#response").css('display','block');
			$("#loader").css('display','block');
			$("#submit_button").attr('disabled','true').css({'background-color':'#8ABA56','color':'#FFF','opacity':.6});
			var formData =$('#login_form').serialize();
			var url =HTTP_HOST+"ajax_request/process_login";
			$.ajax({
				  type: "POST",
				  url: url,
				  data: formData,
				  success: function(data){
					var result = $.parseJSON(data);
					if(result.success==true){
						$("#loader").css('display','none');
						$("#response").text(result.text).css('color','green').fadeOut(5000,function(){ $("#response").text('Please wait..');$('#reset_button').trigger('click'); });
						$("#submit_button").removeAttr('disabled').css({'background-color':'#FFF','color':'#000','opacity':1});
						$("#reset_button").css({'opacity':1});
						window.location.href=HTTP_HOST+'welcome';
					}else{
						$("#loader").css('display','none');
						$("#response").text(result.text).css({'color':'red'}).fadeOut(5000,function(){ $("#response").text('Please wait..'); });
						$("#submit_button").removeAttr('disabled').css({'background-color':'#FFF','color':'#000','opacity':1});	
						$("#reset_button").css({'opacity':1});
					}
				  }
			});
			
		}
	</script>
  </body>
