<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<title>RMS | SubAdmin Dashboard !!</title>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="container">
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
      <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <?php echo left_menu('welcome'); ?>
        <!-- END SIDEBAR MENU -->
      </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
              </div>
              <div class="modal-body">
                 Widget settings form goes here
              </div>
              <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">Dashboard</h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="index.html">Home</a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="#">Dashboard</a>
            </li>
          </ul>
          <div class="page-toolbar">
            <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
              <i class="icon-calendar"></i>&nbsp;&nbsp; 
              <i class="fa fa-angle-down"></i>
              <!-- uncomment this to display selected daterange in the button 
              &nbsp; <span class="thin uppercase visible-lg-inline-block"></span>&nbsp;
              <i class="fa fa-angle-down"></i>
              -->
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN DASHBOARD STATS -->
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light blue-soft" href="#">
            <div class="visual">
              <i class="fa fa-comments"></i>
            </div>
            <div class="details">
              <div class="number">
                 1349
              </div>
              <div class="desc">
                 New Feedbacks
              </div>
            </div>
            </a>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light red-soft" href="#">
            <div class="visual">
              <i class="fa fa-trophy"></i>
            </div>
            <div class="details">
              <div class="number">
                 12,5M$
              </div>
              <div class="desc">
                 Total Profit
              </div>
            </div>
            </a>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light green-soft" href="#">
            <div class="visual">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
              <div class="number">
                 549
              </div>
              <div class="desc">
                 New Orders
              </div>
            </div>
            </a>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light purple-soft" href="#">
            <div class="visual">
              <i class="fa fa-globe"></i>
            </div>
            <div class="details">
              <div class="number">
                 +89%
              </div>
              <div class="desc">
                 Brand Popularity
              </div>
            </div>
            </a>
          </div>
        </div>
        <!-- END DASHBOARD STATS -->
        <div class="clearfix">
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
              <div class="portlet-title">
                <div class="caption">
                  <i class="icon-bar-chart font-green-sharp hide"></i>
                  <span class="caption-subject font-green-sharp bold uppercase">Site Visits</span>
                  <span class="caption-helper">weekly stats...</span>
                </div>
                <div class="actions">
                  <div class="btn-group btn-group-devided" data-toggle="buttons">
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                    <input type="radio" name="options" class="toggle" id="option1">New</label>
                    <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
                    <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                  </div>
                </div>
              </div>
              <div class="portlet-body">
                <div id="site_statistics_loading">
                  <img src="<?php echo base_url(); ?>assets/admin/layout2/img/loading.gif" alt="loading"/>
                </div>
                <div id="site_statistics_content" class="display-none">
                  <div id="site_statistics" class="chart">
                  </div>
                </div>
              </div>
            </div>
            <!-- END PORTLET-->
          </div>
          <div class="col-md-6 col-sm-6">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
              <div class="portlet-title">
                <div class="caption">
                  <i class="icon-share font-red-sunglo hide"></i>
                  <span class="caption-subject font-red-sunglo bold uppercase">Revenue</span>
                  <span class="caption-helper">monthly stats...</span>
                </div>
                <div class="actions">
                  <div class="btn-group">
                    <a href="" class="btn grey-salsa btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    Filter Range&nbsp;<span class="fa fa-angle-down">
                    </span>
                    </a>
                    <ul class="dropdown-menu pull-right">
                      <li>
                        <a href="javascript:;">
                        Q1 2014 <span class="label label-sm label-default">
                        past </span>
                        </a>
                      </li>
                      <li>
                        <a href="javascript:;">
                        Q2 2014 <span class="label label-sm label-default">
                        past </span>
                        </a>
                      </li>
                      <li class="active">
                        <a href="javascript:;">
                        Q3 2014 <span class="label label-sm label-success">
                        current </span>
                        </a>
                      </li>
                      <li>
                        <a href="javascript:;">
                        Q4 2014 <span class="label label-sm label-warning">
                        upcoming </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="portlet-body">
                <div id="site_activities_loading">
                  <img src="<?php echo base_url(); ?>assets/admin/layout2/img/loading.gif" alt="loading"/>
                </div>
                <div id="site_activities_content" class="display-none">
                  <div id="site_activities" style="height: 228px;">
                  </div>
                </div>
                <div style="margin: 20px 0 10px 30px">
                  <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                      <span class="label label-sm label-success">
                      Revenue: </span>
                      <h3>$13,234</h3>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                      <span class="label label-sm label-danger">
                      Shipment: </span>
                      <h3>$1,134</h3>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                      <span class="label label-sm label-primary">
                      Orders: </span>
                      <h3>235090</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END PORTLET-->
          </div>
        </div>
        
        <div class="clearfix">
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <div class="portlet light ">
              <div class="portlet-title">
                <div class="caption">
                  <i class="icon-cursor font-purple-intense hide"></i>
                  <span class="caption-subject font-purple-intense bold uppercase">General Stats</span>
                </div>
                <div class="actions">
                  <a href="javascript:;" class="btn btn-sm btn-circle btn-default easy-pie-chart-reload">
                  <i class="fa fa-repeat"></i> Reload </a>
                </div>
              </div>
              <div class="portlet-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="easy-pie-chart">
                      <div class="number transactions" data-percent="55">
                        <span>
                        +55 </span>
                        %
                      </div>
                      <a class="title" href="#">
                      Transactions <i class="icon-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                  <div class="margin-bottom-10 visible-sm">
                  </div>
                  <div class="col-md-4">
                    <div class="easy-pie-chart">
                      <div class="number visits" data-percent="85">
                        <span>
                        +85 </span>
                        %
                      </div>
                      <a class="title" href="#">
                      New Visits <i class="icon-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                  <div class="margin-bottom-10 visible-sm">
                  </div>
                  <div class="col-md-4">
                    <div class="easy-pie-chart">
                      <div class="number bounce" data-percent="46">
                        <span>
                        -46 </span>
                        %
                      </div>
                      <a class="title" href="#">
                      Bounce <i class="icon-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="portlet light ">
              <div class="portlet-title">
                <div class="caption">
                  <i class="icon-equalizer font-purple-plum hide"></i>
                  <span class="caption-subject font-red-sunglo bold uppercase">Server Stats</span>
                  <span class="caption-helper">monthly stats...</span>
                </div>
                <div class="tools">
                  <a href="" class="collapse">
                  </a>
                  <a href="#portlet-config" data-toggle="modal" class="config">
                  </a>
                  <a href="" class="reload">
                  </a>
                  <a href="" class="remove">
                  </a>
                </div>
              </div>
              <div class="portlet-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="sparkline-chart">
                      <div class="number" id="sparkline_bar">
                      </div>
                      <a class="title" href="#">
                      Network <i class="icon-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                  <div class="margin-bottom-10 visible-sm">
                  </div>
                  <div class="col-md-4">
                    <div class="sparkline-chart">
                      <div class="number" id="sparkline_bar2">
                      </div>
                      <a class="title" href="#">
                      CPU Load <i class="icon-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                  <div class="margin-bottom-10 visible-sm">
                  </div>
                  <div class="col-md-4">
                    <div class="sparkline-chart">
                      <div class="number" id="sparkline_line">
                      </div>
                      <a class="title" href="#">
                      Load Rate <i class="icon-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix">
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <!--Cooming Soon...-->
    <!-- END QUICK SIDEBAR -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <div class="page-footer">
    <div class="page-footer-inner">
       2016 &copy; RMS Construction by pscreations.
    </div>
    <div class="scroll-to-top">
      <i class="icon-arrow-up"></i>
    </div>
  </div>
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-validation.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-samples.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	jQuery(document).ready(function() {    
	   Metronic.init(); 	// init metronic core componets
	   Layout.init(); 		// init layout
	   Demo.init(); 		// init demo features 
	   Index.init();   
	   Index.initDashboardDaterange();
	   Index.initCharts(); 	// init index page's custom scripts
	   Index.initChat();
	   Index.initMiniCharts();
	   Tasks.initDashboardWidget();
     FormValidation.init();
     FormSamples.init();
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
