<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<title>Manage Users | RMS Construction !!</title>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="container">
  <div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
      <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <?php echo left_menu('users'); ?>
        <!-- END SIDEBAR MENU -->
      </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade bs-modal-lg" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="portlet box blue-madison">
                        <div class="portlet-title">
                            <div class="caption">Update Product</div>
                            <div class="tools">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closemodalbutton"></button>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" id="update_product_form" class="form-horizontal">
                            <input type="hidden" name="productId" id="productId" />
                            <input type="hidden" name="type" value="update" />
                            <div class="form-body">
                                
                                <div class="alert alert-danger danger-update-products display-hide">
                                    <button class="close" data-close="alert"></button>
                                    You have some form errors. Please check below.
                                </div>
                                
                                <div class="alert alert-success success-update-products display-hide">
                                    <button class="close" data-close="alert"></button>
                                    Your form submitted successfully!
                                </div>

                                <div class="row">
                                    <div  class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Category <span class="required">* </span>
                                            </label>
                                            <div class="col-md-8">
                                                
                                                <select class="form-control select2me majorCategory" onchange="major_category(this.value); return false;" name="majorCategory" id="majorCategory">
                                                    <option value="">Select..</option>
                                                    
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Category 2</label>
                                            <div class="col-md-8">
                                                
                                                <select class="form-control select2me" disabled="disabled" name="minorCategory" id="minorCategory" onchange="minor_category(); return false;">
                                                    <option value="">Select...</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div  class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Category 3</label>
                                            <div class="col-md-8">
                                                
                                                <select class="form-control select2me" disabled="disabled" name="miniCategory" id="miniCategory">
                                                    <option value="">Select...</option>

                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Short Name <span class="required">* </span>
                                            </label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" name="productShortName" id="productShortName" placeholder="Product Short Name" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Product Name <span class="required">* </span>
                                            </label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" name="productName" id="productName" placeholder="Product Name" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Measuring Unit <span class="required">* </span>
                                            </label>
                                            <div class="col-md-8"> 
                                                <select class="form-control select2me" name="measuringUnit" id="measuringUnit">
                                                    <option value="">Select...</option>
                                                    <option value="nos">NOS</option>
                                                    <option value="kg">KG</option>
                                                    <option value="mtrs">MTRS</option>
                                                    <option value="sqmtrs">SQ MTRS</option>
                                                    <option value="cum">CUM</option>
                                                    <option value="set">SET</option>
                                                    <option value="pair">PAIR</option>
                                                    <option value="each">EACH</option>
                                                    <option value="ltrs">LTRS</option>
                                                    <option value="cft">CFT</option>
                                                    <option value="sqft">SQFT</option>
                                                    <option value="millimeter" >millimeter(mm)</option>
                                                    <option value="centimeter" >centimeter(cm)</option>
                                                    <option value="decimeter" >decimeter(dm)</option>
                                                    <option value="meter" >meter(m)</option>
                                                    <option value="dekameter" >dekameter(dam)</option>
                                                    <option value="hectometer" >hectometer(hm)</option>
                                                    <option value="kilometer" >kilometer(km)</option>

                                                    <option value="sqr_centimeter" >square centimeter(cm2)</option>
                                                    <option value="sqr_decimeter" >square decimeter(dm2)</option>
                                                    <option value="sqr_meter" >square meter(m2)</option>
                                                    <option value="sqr_dekameter" >square dekameter(dm2)</option>
                                                    <option value="sqr_hectometer" >square hectometer(hm2)</option>
                                                    <option value="sqr_kilometer" >square kilometer(km2)</option>

                                                    <option value="milliliter" >milliliter(mL)</option>
                                                    <option value="centiliter" >centiliter(cL)</option>
                                                    <option value="deciliter" >deciliter(dL)</option>
                                                    <option value="liter" >liter(L)</option>
                                                    <option value="dekaliter" >dekaliter(daL)</option>
                                                    <option value="hectoliter" >hectoliter(hL)</option>
                                                    <option value="kiloliter" >kiloliter(kL)</option>
                                                    
                                                    <option value="cb_millimeter" >cubic millimeter(mm3)</option>
                                                    <option value="cb_centimeter" >cubic centimeter(cm3)</option>
                                                    <option value="cb_decimeter" >cubic decimeter(dm3)</option>
                                                    <option value="cb_meter" >cubic meter(m3)</option>

                                                    <option value="milligram" >milligram(mg)</option>
                                                    <option value="centigram" >centigram(cg)</option>
                                                    <option value="decigram" >decigram(dg)</option>
                                                    <option value="meter" >gram(g)</option>
                                                    <option value="dekagram" >dekagram(dag)</option>
                                                    <option value="hectogram" >hectogram(hg)</option>
                                                    <option value="kilogram" >kilogram(kg)</option>
                                                    <option value="megagram" >megagram(Mg)</option>
                                                    <option value="metricton" >metric tom(t)</option>

                                                    <option value="inch">inches(in)</option>
                                                    <option value="foot">foot(ft)</option>
                                                    <option value="yard">yard(yd)</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Number Of Quantity<span class="required">* </span></label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" name="quantity" id="quantity" class="form-control" placeholder="#Number" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Price Per Unit<span class="required">* </span></label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" name="pricePerUnit"  id="pricePerUnit" class="form-control" placeholder="Price Per Unit" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Total Price<span class="required">* </span></label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" name="totalPrice" id="totalPrice" readonly="readonly" class="form-control" placeholder="Total Price" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                                <!--/row-->
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit"  class="btn green">Update</button>
                                            <button type="button" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>              
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">Products Management</h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="fa fa-home"></i>
              <a href="<?php echo base_url();?>welcome">Home</a>
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="<?php echo base_url();?>products">Products</a>
            </li>
          </ul>
          
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN DASHBOARD STATS -->
        
        <!-- VIEW USERS -->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Product 
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <!-- <a href="javascript:;" class="remove">
                    </a> -->
                </div>
            </div>
            <div class="portlet-body">

                <div class="alert alert-danger danger-remove-product display-hide">
                    <button class="close" data-close="alert"></button>
                    Product not removed due to some error, Please try after sometime.
                </div>
                
                <div class="alert alert-success success-remove-product display-hide">
                    <button class="close" data-close="alert"></button>
                    Product remove successfully!
                </div>

                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    
                </table> 
                
            </div>
        </div>
        <!-- END VIEW USERS --> 

            
        <!-- END DASHBOARD STATS -->
        
        
        
        
      </div>
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <!--Cooming Soon...-->
    <!-- END QUICK SIDEBAR -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <div class="page-footer">
    <div class="page-footer-inner">
       2016 &copy; RMS Construction by pscreations.
    </div>
    <div class="scroll-to-top">
      <i class="icon-arrow-up"></i>
    </div>
  </div>
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-validation.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-samples.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/product-listing.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-products.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
    // initiate layout and plugins
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Demo.init(); // init demo features
    FormValidation.init();
    FormSamples.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
