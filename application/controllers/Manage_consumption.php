<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class manage_consumption extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('manage_consumption_model');					
	}
	
	public function index(){
		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==1){ 
	 		$this->load->view('include/header');
			$this->load->view('manageconsumption');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	

	function getProductList(){
		return $this->manage_consumption_model->getActiveProducts();
	}

	function saveProductConsumption(){
		return $this->manage_consumption_model->saveProductConsumption();	
	}


	
}