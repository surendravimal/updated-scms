<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class arrived_products extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('arrived_products_model');					
	}
	
	public function index(){
		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==1){ 
			$this->load->view('include/header');
			$this->load->view('arrivedproducts');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	/*function getSiteInChargeList(){
		return $this->manage_product_model->getSiteInChargeList();
	}*/
	function getProductList(){
		return $this->arrived_products_model->getArrivedProducts();
	}

	/*function saveSiteInChargeProducts(){
		return $this->manage_product_model->saveSiteInChargeProducts();	
	}*/


	
}