<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Controller {
	
	
	public function __construct(){
		parent::__construct();
		//$this->load->helper('url');
		//$this->load->helper('left_menu');
		$this->load->model('user_model');
	}
	
	function token($param){
		if($param !=''){
			$response = $this->user_model->authenticate($param);
			$data = array();
			$data['success'] = $response;
			//$data['success'] = "True";
			$this->load->view('include/loginheader');
			$this->load->view('login_soft',$data);
			$this->load->view('include/loginfooter');
			
		}
	}
}