<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('user_model');					
	}
	
	public function index(){
		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99){ 
	 		$this->load->view('include/header');
			$this->load->view('users');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	function addUser(){
		return $this->user_model->registration();
	}

	function updateUser($userId,$status){
	}

	function getUserList(){
		echo json_encode($this->user_model->getUserListing());
		
	}

	
}