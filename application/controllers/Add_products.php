<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_products extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('user_model');
		$this->load->model('product_model');					
	}
	
	public function index(){
		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==1 || $this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99 ){
			$this->load->view('include/header');
			$this->load->view('add_product');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	function getMajorCate(){
		$majorCate = $this->product_model->getMajorCate();
		if(!empty($majorCate)){
			echo json_encode(array('success'=>true,'data'=>$majorCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}

	function getMinorCate(){
		$majorCateId = $_POST['majorCateId'];
		$minorCate = $this->product_model->getMinorCate($majorCateId);
		if(!empty($minorCate)){
			echo json_encode(array('success'=>true,'data'=>$minorCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}


	function getMinorCate2(){
		$minorCate = $this->product_model->getMinorCate2();
		if(!empty($minorCate)){
			echo json_encode(array('success'=>true,'data'=>$minorCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}

	function getMiniCate(){
		$majorCateId = $_POST['majorCateId'];
		$minorCateId = $_POST['minorCateId'];
		$miniCate = $this->product_model->getMiniCate($majorCateId,$minorCateId);
		if(!empty($miniCate)){
			echo json_encode(array('success'=>true,'data'=>$miniCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}
	

	function getMiniCate2(){
		$miniCate = $this->product_model->getMiniCate2();
		if(!empty($miniCate)){
			echo json_encode(array('success'=>true,'data'=>$miniCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}


	function addMajorCategory(){
		return $this->product_model->addMajorCategory();
	}

	function addMinorCategory(){
		//one variabl---
		return $this->product_model->addMinorCategory();
	}

	function addMiniCategory(){
		//two variabl---
		return $this->product_model->addMiniCategory();
	}

	function addNewProduct(){
		return $this->product_model->addNewProduct();	
	}

	function deleteMajorCategory(){
		return $this->product_model->deleteMajorCategory();
	}


	function deleteMinorCategory(){
		return $this->product_model->deleteMinorCategory();
	}


	function deleteMiniCategory(){
		return $this->product_model->deleteMiniCategory();
	}

	
}