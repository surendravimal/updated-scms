<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();				
	}

	public function index(){

		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==1){ 			// for Site-InCharge 
			$this->load->view('include/header');
			$this->load->view('siteInCharge');
			$this->load->view('include/footer');
		}else if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==51){  	// for SubAdmin
			$this->load->view('include/header');
			$this->load->view('subAdmin');
			$this->load->view('include/footer');
		}else if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==99){  	// for SuperAdmin
			$this->load->view('include/header');
			$this->load->view('superAdmin');
			$this->load->view('include/footer');
		}else{
			$this->load->view('include/loginheader');
			$this->load->view('login_soft');
			$this->load->view('include/loginfooter');
		}
	}
}
