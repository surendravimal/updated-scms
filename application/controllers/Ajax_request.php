<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_request extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

	
	function process_registration(){
		$response = $this->form_model->registration();
		echo $response;
	}
	
	function process_contactus(){
		$response = $this->form_model->contactus();
		echo $response;
	}
	
	function process_login(){
		$response = $this->user_model->do_login();
		echo $response;
	}
	
	function process_forget_password(){
		$response = $this->user_model->forget_password();
		echo $response;
	}


	function process_reset_password(){
		$response = $this->form_model->reset_password();
		echo $response;
	}
	
	function process_review(){
		$response = $this->form_model->review_submit();
		echo $response;
	}
	
	function process_upload_paper(){
		if($this->session->userdata('logged_in')==1){
			$response = $this->form_model->upload_paper();
			echo $response;
		}else{
			redirect('login');
		}
	
	}
}