<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('welcome_model');				
	}
	
	public function index(){
		
		$updateLogs = $this->welcome_model->update_logs();

		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==1){ 			// for Site-InCharge 
			$this->load->view('include/header');
			$this->load->view('siteInCharge');
			$this->load->view('include/footer');
		}else if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==51){  	// for SubAdmin
			$this->load->view('include/header');
			$this->load->view('subAdmin');
			$this->load->view('include/footer');
		}else if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==99){  	// for SuperAdmin
			$this->load->view('include/header');
			$this->load->view('superAdmin');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	function logout(){
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect(base_url().'login');
	}
}