<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class manage_user extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('user_model');					
	}
	
	public function index(){

		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99){ 
	 		$this->load->view('include/header');
			$this->load->view('manageuser');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	//Type = get , update
	function updateUser(){
	
		$userId = $_POST['userId'];
		$type = $_POST['type'];
		
		if($type=='get'){
			return $this->user_model->getUser($userId);
		}
		if($type=='update'){
			return $this->user_model->updateUser($userId);
		}
	}

	function removeUser(){
		$userId = $_POST['userId'];
		return $this->user_model->removeUser($userId);
	}

	function getUserList(){
		return $this->user_model->getUserListing();
	}

	function processResetPassword(){
		return $this->user_model->resetPassword();
	}

	
}