<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class manage_product extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('manage_product_model');					
	}
	
	public function index(){
		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99){ 

			$data['userList'] = $this->manage_product_model->getSiteInChargeList();
			
	 		$this->load->view('include/header');
			$this->load->view('manageproduct',$data);
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	function getSiteInChargeList(){
		return $this->manage_product_model->getSiteInChargeList();
	}
	function getProductList(){
		return $this->manage_product_model->getActiveProducts();
	}

	function saveSiteInChargeProducts(){
		return $this->manage_product_model->saveSiteInChargeProducts();	
	}


	
}