<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class products extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('product_model');					
	}
	
	public function index(){

		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==1 || $this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99){ 
	 		$this->load->view('include/header');
			$this->load->view('products');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	//Type = get , update
	function updateProduct(){
		$productId = $_POST['productId'];
		$type = $_POST['type'];
		
		if($type=='get'){
			return $this->product_model->getProduct($productId);
		}

		if($type=='update'){
			return $this->product_model->updateProduct($productId);
		}
	}

	function removeProduct(){
		$productId = $_POST['productId'];
		return $this->product_model->removeProduct($productId);
	}

	function getProductList(){
		return $this->product_model->getProductListing();
	}


	function changeStatus(){
		$status = $_POST['status'];
		$productId = $_POST['productId'];
		return $this->product_model->changeStatus($status,$productId);
	}

	

	
}