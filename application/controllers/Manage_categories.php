<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_categories extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('product_model');					
	}
	
	public function index(){
		if($this->session->userdata('logged_in')==1 && $this->session->userdata('privilege')==1 || $this->session->userdata('privilege')==51 || $this->session->userdata('privilege')==99 ){
			$this->load->view('include/header');
			$this->load->view('managecategories');
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	function getMajorCate(){
		$majorCate = $this->product_model->getMajorCate();
		if(!empty($majorCate)){
			echo json_encode(array('success'=>true,'data'=>$majorCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}

	function getMinorCate(){
		$majorCateId = $_POST['majorCateId'];
		$minorCate = $this->product_model->getMinorCate($majorCateId);
		if(!empty($minorCate)){
			echo json_encode(array('success'=>true,'data'=>$minorCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}


	function getMiniCate(){
		$majorCateId = $_POST['majorCateId'];
		$minorCateId = $_POST['minorCateId'];
		$miniCate = $this->product_model->getMiniCate($majorCateId,$minorCateId);
		if(!empty($miniCate)){
			echo json_encode(array('success'=>true,'data'=>$miniCate));	
		}else{
			echo json_encode(array('success'=>false,'data'=>''));
		}
	}
	


	function addUpdateMajorCategory(){
		return $this->product_model->addUpdateMajorCategory();
	}

	function addUpdateMinorCategory(){
		//one variabl---
		return $this->product_model->addUpdateMinorCategory();
	}

	function addUpdateMiniCategory(){
		//two variabl---
		return $this->product_model->addUpdateMiniCategory();
	}

	

	
}