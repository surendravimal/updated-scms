<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('left_menu');
		$this->load->model('user_model');					
	}
	
	public function index(){
		if($this->session->userdata('logged_in')==1){

			$profileDetails = $this->user_model->getUserProfile($this->session->userdata('userId'));
			$this->load->view('include/header');
			$this->load->view('profile',$profileDetails);
			$this->load->view('include/footer');
		}else{
			redirect('login');
		}
	}

	function updateProfile(){
		return $this->user_model->updateUserProfile();
		
	}

	
}