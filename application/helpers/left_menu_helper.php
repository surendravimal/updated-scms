<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  if (!function_exists('left_menu')){
    
    function left_menu($controller){
        $obj =& get_instance();
        $obj->load->library('session');
      
        $left_menu='';
        $left_menu='<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">';
      
        if($obj->session->userdata('logged_in')==1 && $obj->session->userdata('privilege')==99){
            $left_menu.= '<li '.(($controller=="welcome")?'class="start active"':'').'>
                            <a href="'.base_url().'welcome"><i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span '.(($controller=="welcome")?'class="selected"':'').'></span>
                            </a>
                          </li>';
            $left_menu.= '<li '.(($controller=="users")?'class="active"':'').'>
                            <a href="javascript:void(0);"><i class="icon-user"></i>
                                <span class="title">users</span>
                                <span '.(($controller=="users")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="'.base_url().'users">Add User</a></li>
                                <li><a href="'.base_url().'manage_user">Manage Users</a></li>
                            </ul>
                          </li>';
            $left_menu.= '<li '.(($controller=="products")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-basket"></i>
                                <span class="title">Products</span>
                                <span '.(($controller=="products")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <!-- <li><a href="'.base_url().'welcome"><i class="icon-basket"></i>Create New Order</a></li>
                                <li><a href="'.base_url().'welcome"><i class="icon-tag"></i>Process Orders</a></li> -->
                                <li><a href="'.base_url().'add_products"><i class="icon-basket"></i>Add Products</a></li>
                                <li><a href="'.base_url().'products"><i class="icon-handbag"></i>Products Listing</a></li>
                                <li><a href="'.base_url().'manage_product"><i class="icon-handbag"></i>Manage Products</a></li>
                            </ul>
                          </li>';
            $left_menu.= '<li '.(($controller=="mcategory")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-basket"></i>
                                <span class="title">Manage Category</span>
                                <span '.(($controller=="mcategory")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="'.base_url().'manage_categories"><i class="icon-handbag"></i>Manage Category</a></li>
                            </ul>
                          </li>';
            $left_menu.= '<li '.(($controller=="reports")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-briefcase"></i>
                                <span class="title">Reports</span>
                                <span '.(($controller=="reports")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="'.base_url().'welcome">Products In Inventory</a></li>
                                <li><a href="'.base_url().'welcome">Products At Sites</a></li>
                                <li><a href="'.base_url().'welcome">Consumed Products</a></li>
                            </ul>
                          </li>';
      
        }else if($obj->session->userdata('logged_in')==1 && $obj->session->userdata('privilege')==51) { 
            $left_menu.= '<li '.(($controller=="welcome")?'class="start active"':'').'>
                            <a href="'.base_url().'welcome"><i class="icon-home"></i>
                              <span class="title">Dashboard</span><span '.(($controller=="welcome")?'class="selected"':'').'></span>
                            </a>
                          </li>';
            $left_menu.= '<li '.(($controller=="users")?'class="active"':'').'>
                            <a href="javascript:void(0);"><i class="icon-user"></i>
                                <span class="title">users</span>
                                <span '.(($controller=="users")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="'.base_url().'manage_user">Manage Users</a></li>
                            </ul>
                          </li>';
            $left_menu.= '<li '.(($controller=="products")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-basket"></i>
                                <span class="title">Products</span>
                                <span '.(($controller=="products")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <!-- <li><a href="'.base_url().'welcome"><i class="icon-basket"></i>Create New Order</a></li>
                                <li><a href="'.base_url().'welcome"><i class="icon-tag"></i>Process Order</a></li> -->
                                <li><a href="'.base_url().'add_products"><i class="icon-basket"></i>Add Products</a></li>
                                <li><a href="'.base_url().'products"><i class="icon-handbag"></i>Products Listing</a></li>
                                <li><a href="'.base_url().'manage_product"><i class="icon-handbag"></i>Manage Products</a></li>
                            </ul>
                          </li>';
            $left_menu.= '<li '.(($controller=="mcategory")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-basket"></i>
                                <span class="title">Manage Category</span>
                                <span '.(($controller=="mcategory")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="'.base_url().'manage_categories"><i class="icon-handbag"></i>Manage Category</a></li>
                            </ul>
                          </li>';
            $left_menu.= '<li '.(($controller=="reports")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-briefcase"></i>
                                <span class="title">Reports</span>
                                <span '.(($controller=="reports")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="'.base_url().'welcome">Products In Inventory</a></li>
                                <li><a href="'.base_url().'welcome">Products at Sites</a></li>
                                <li><a href="'.base_url().'welcome">Consumed Products</a></li>
                                
                            </ul>
                          </li>';
        }else{
            $left_menu.= '<li '.(($controller=="welcome")?'class="start active"':'').'>
                            <a href="'.base_url().'welcome"><i class="icon-home"></i>
                              <span class="title">Dashboard</span><span '.(($controller=="welcome")?'class="selected"':'').'></span>
                            </a>
                          </li>';
            $left_menu.= '<li '.(($controller=="products")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-basket"></i>
                                <span class="title">Products</span>
                                <span '.(($controller=="products")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                              <!-- <li><a href="'.base_url().'welcome"><i class="icon-basket"></i>Create New Order</a></li>
                              <li><a href="'.base_url().'welcome"><i class="icon-tag"></i>Order Status</a></li> -->
                              <li><a href="'.base_url().'add_products"><i class="icon-basket"></i>Add Products</a></li>   
                              <li><a href="'.base_url().'products"><i class="icon-handbag"></i>Products Listing</a></li>
                              <li><a href="'.base_url().'arrived_products"><i class="icon-basket"></i>Arrived Products</a></li>
                              <li><a href="'.base_url().'manage_consumption"><i class="icon-handbag"></i>Manage Consumption</a></li>
                            </ul>
                          </li>';
            $left_menu.= '<li '.(($controller=="reports")?'class="active"':'').'>
                            <a href="javascript:;"><i class="icon-briefcase"></i>
                                <span class="title">Reports</span>
                                <span '.(($controller=="reports")?'class="selected"':'').'></span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                              <li><a href="'.base_url().'welcome">Products at Site</a></li>
                              <li><a href="'.base_url().'welcome">Consumed Products</a></li>
                              <li><a href="'.base_url().'welcome">Remaining Products</a></li>
                            </ul>
                          </li>';
        }
            
      $left_menu.='</ul>';
      return $left_menu;
    }
  }
  
  
  
  if(!function_exists('uploadFile')){
    function uploadFile($data,$name){
      $error='';
      $success='';
      $client_name=$name;
      $upload_path = './papers/';
      
      $file_temp = $data['tmp_name'];
      $file_size = $data['size']/1024;
      $file_mime_type= $data['type'];
      $file_type = preg_replace("/^(.+?);.*$/", "\\1", $file_mime_type);
      $file_type = strtolower(trim(stripslashes($file_type), '"'));
      $file_name = $data['name'];
      $file_ext = get_extension($file_name);
      
      
      $file_mimes = array('.doc','.docx');
      $is_file = (in_array($file_ext, $file_mimes, TRUE)) ? TRUE : FALSE;
      if($file_size<102400){
        if($is_file ==1){
          move_uploaded_file($data['tmp_name'],$upload_path.$client_name.$file_ext);
          $success['text']="File Uploaded Successfully";
          $success['file_name']=$client_name.$file_ext;
        }else{
          $error['text']="Invalide FileType";
        }
        
      }else{
        $error['text']="Invalide File Size";
      }
      
      if (!empty($error)){
          return  array('error' =>$error);
      }else{
          return array('result'=>$success);
      }
    }
  }
  
  
  if(!function_exists('uploadFile2')){
    function uploadFile2($data,$name){
      $error='';
      $success='';
      $client_name=$name;
      $upload_path = './papers/pdf/';
      
      $file_temp = $data['tmp_name'];
      $file_size = $data['size']/1024;
      $file_mime_type= $data['type'];
      $file_type = preg_replace("/^(.+?);.*$/", "\\1", $file_mime_type);
      $file_type = strtolower(trim(stripslashes($file_type), '"'));
      $file_name = $data['name'];
      $file_ext = get_extension($file_name);
      
      
      $file_mimes = array('.pdf');
      $is_file = (in_array($file_ext, $file_mimes, TRUE)) ? TRUE : FALSE;
      if($file_size<102400){
        if($is_file ==1){
          move_uploaded_file($data['tmp_name'],$upload_path.$client_name.$file_ext);
          $success['text']="File Uploaded Successfully";
          $success['file_name']=$client_name.$file_ext;
        }else{
          $error['text']="Invalide FileType";
        }
        
      }else{
        $error['text']="Invalide File Size";
      }
      
      if (!empty($error)){
         return  array('error' =>$error);
      }else{
        return array('result'=>$success);
      }
    }
  }
  
  function get_extension($filename){
    $x = explode('.', $filename);
    return '.'.end($x);
  }

?>