$(document).ready(function(){
	refreshMajorCate();
	refreshMinorCate();
	refreshMiniCate();
});

function addMajorCategory(){

	if($('#addcate1').val() != '' ){
		var category = $('#addcate1').val();
		var formdata = {'majorCateName':category};
		$.ajax({
			'url':HTTP_HOST+'Add_products/addMajorCategory',
			'type':'POST',
			'data':formdata,
			'success':function(data){
				var result = $.parseJSON(data);
				if(result.success==true){

					$(".majorCategory").select2("val", "");
					TableEditable.destroy1();
	                refreshMajorCate();
	                $('#addcate1').val('');
	            }else{
	            	alert(result.text);
	            }
			}
		});

	}else{
		alert('Please enter category name');
	}
}


function addMinorCategory(){
	var majorCategoryId = $('#majorCateIdForMinor').val();
	if(majorCategoryId !=''){
		var category = $('#addcate2').val();
		var formdata = {'minorCateName':category,'majorCateId':majorCategoryId};
		$.ajax({
			'url':HTTP_HOST+'Add_products/addMinorCategory',
			'type':'POST',
			'data':formdata,
			'success':function(data){
				var result = $.parseJSON(data);
				if(result.success==true){
					TableEditable.destroy2();
	                refreshMinorCate();
	                $('#addcate2').val('');
	               
	            }else{
	                alert(result.text);
	            }
			}
		});
	}else{
		alert('Please select main category first!');
	}
	
}


function addMiniCategory(){
	var majorCategoryId = $('#majorCateIdForMini').val();
	var minorCategoryId = $('#minorCateIdForMini').val();
	var category = $('#addcate3').val();
	if(majorCategoryId !=''){
		if(minorCategoryId !=''){
			var formdata = {'miniCateName':category,'majorCateId':majorCategoryId,'minorCateId':minorCategoryId};
			$.ajax({
				'url':HTTP_HOST+'Add_products/addMiniCategory',
				'type':'POST',
				'data':formdata,
				'success':function(data){
					var result = $.parseJSON(data);
					if(result.success==true){
		                alert(result.text);
		                TableEditable.destroy3();
		            	refreshMiniCate();
		            	$('#addcate3').val('');
		            }else{
		                alert(result.text);
		            }
				}
			});
		}else{
			alert('Please select sub category first!');
		}
	}else{
		alert('Please select main category first!');
	}
	
}


function refreshMajorCate(){
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMajorCate',
		'type':'POST',
		'success':function(data){
			var result = $.parseJSON(data);
			var html =''; 
			if(result.success==true){
				for(var i = 0; i<result.data.length;i++){
					var j = i+1;
					html +='<tr>';
					html +='<td><input type="hidden" value="'+result.data[i].majorCateId+'" />'+j+'</td>';
					html +='<td>'+result.data[i].majorCateName+'</td>';
					//html +='<td><a class="edit" href="javascript:;">Edit </a></td>';
					html +='<td><a class="delete" href="javascript:;">Delete </a></td>';
					html +='</tr>';
					
				}

				$('#majorCategoryList').html(html);
				TableEditable.category();

				$(".majorCategory").select2("val", "");
				var html1 ='<option value="">Select...</option>';
				for(var i = 0; i<result.data.length;i++){
					html1 +='<option value="'+result.data[i].majorCateId+'">'+result.data[i].majorCateName+'</option>'
				}
				$('#majorCateIdForMinor').html(html1);
				
				$('#majorCateIdForMini').html(html1);
				
            }else{
            
            }
		}
	});
}

function refreshMinorCate(){
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMinorCate2',
		'type':'POST',
		'success':function(data){
			var result = $.parseJSON(data);
			var html =''; 
			if(result.success==true){
				for(var i = 0; i<result.data.length;i++){
					var j = i+1;
					html +='<tr>';
					html +='<td><input type="hidden" value="'+result.data[i].minorCateId+'" />'+j+'</td>';
					html +='<td>'+result.data[i].minorCateName+'</td>';
					html +='<td>'+result.data[i].majorCateName+'</td>';
					//html +='<td><a class="edit" href="javascript:;">Edit </a></td>';
					html +='<td><a class="delete" href="javascript:;">Delete </a></td>';
					html +='</tr>';
					
				}

				$('#majorCategoryList2').html(html);
				//$('#majorCategoryList3').html(html);
				TableEditable.category2();
				$(".majorCategory").select2("val", "");
				$('select[name="minorCategory"]').removeAttr('disabled');
				var html ='<option value="">Select..</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].minorCateId+'">'+result.data[i].minorCateName+'</option>'
				}
				$('#minorCateIdForMini').html(html);
				
            }else{
            
            }
		}
	});
}

function refreshMiniCate(value0,value){
	
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMiniCate2',
		'type':'POST',
		'success':function(data){
			var result = $.parseJSON(data);
			var html =''; 
			if(result.success==true){
				for(var i = 0; i<result.data.length;i++){
					var j = i+1;
					html +='<tr>';
					html +='<td><input type="hidden" value="'+result.data[i].miniCateId+'" />'+j+'</td>';
					html +='<td>'+result.data[i].miniCateName+'</td>';
					html +='<td>'+result.data[i].minorCateName+'</td>';
					html +='<td>'+result.data[i].majorCateName+'</td>';
					//html +='<td><a class="edit" href="javascript:;">Edit </a></td>';
					html +='<td><a class="delete" href="javascript:;">Delete </a></td>';
					html +='</tr>';
					
				}

				$('#majorCategoryList3').html(html);
				TableEditable.category3();
				
            }else{
            
            }
		}
	});
}

function refreshMinorCate2(value){
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMinorCate',
		'type':'POST',
		'data':{'majorCateId':value},
		'success':function(data){
			var result = $.parseJSON(data);
			
			if(result.success==true){
				$('select[name="minorCategory"]').removeAttr('disabled');
				var html ='<option value="">Select..</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].minorCateId+'">'+result.data[i].minorCateName+'</option>'
				}
				//$('#minorCategory').html(html);
				$('#minorCateIdForMini').html(html);

            }else{
            	$('#minorCategory').attr('disabled',true);	
				$('#minorCategory').html('<option value="">No Category Found</option>');
            }

            $("#minorCategory").select2("val", "");
		}
	});
}

function refreshMiniCate2(value0,value){
	
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMiniCate',
		'type':'POST',
		'data':{'majorCateId':value0,'minorCateId':value},
		'success':function(data){
			var result = $.parseJSON(data);
			
			if(result.success==true){
				$('select[name="miniCategory"]').removeAttr('disabled');
				var html ='<option value="">Select..</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].miniCateId+'">'+result.data[i].miniCateName+'</option>'
				}
				$('#miniCategory').html(html);
            }else{
            	$('select[name="miniCategory"]').attr('disabled',true);	
				$('select[name="miniCategory"]').html('<option value="">No Category Found</option>');
            }
            $("#miniCategory").select2("val", "");
		}
	});
}

