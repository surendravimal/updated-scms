// JavaScript Document
var oTable = "";
var checkedItems = new Array();
var check = true;
var counter=1;


/*var persistChecked = function(){
	$('input[type="checkbox"][name="productCheckBox"]','.datatable').each(function(){
		if (checkedItemsContains($(this).attr('id'))){
			$(this).attr('checked','checked');
		}else{
		 //$(this).removeAttr('checked');
		}
	});
}*/


$(document).ready(function(){
	loadproducts();
});

function loadproducts(){
	var aoColumns = [
				{"aaData": "#", "sTitle":"#","sWidth":"1%"},
				{"aaData": "hidden", "sTitle":"hidden","sWidth": "10","bSortable": false,"bVisible": false},
				{"aaData": "Name", "sTitle": "Name","bSortable": true,"sWidth":"14%"},
				{"aaData": "Category1", "sTitle": "Category1","bSortable": true,"sWidth":"12%"},
				{"aaData": "Category2", "sTitle": "Category2","bSortable": false,"sWidth":"10%"},
				{"aaData": "Category3", "sTitle": "Category3","bSortable": false,"sWidth":"10%"},
				{"aaData": "Status", "sTitle": "Status","bSortable": false,"sWidth":"8%"},
				{"aaData": "Quantity", "sTitle": "Quantity","bSortable": false,"sWidth":"10%"},
				{"aaData": "Total Price", "sTitle": "Total Price","bSortable": false,"sWidth":"8%"},
				{"aaData": "Date", "sTitle": "Date","bSortable": false,"sWidth":"11%"},
				{"aaData": "Add By", "sTitle": "Add By","bSortable": true,"sWidth":"10%"},
				{"aaData": "Action", "sTitle": "Action","bSortable": false,"sWidth":"8%"},
				];
	
		oTable = $('#sample_1').dataTable({
			"bProcessing": true,
            "bServerSide": true,
			"bDestroy": true,
			"sAjaxSource": HTTP_HOST+'manage_product/getproductList',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 5,
            "oLanguage": {
                "sProcessing": '<img src="'+HTTP_HOST+'assets/global/img/loader.gif" alt="Processing..">'
            },
			'bAutoWidth': false,
            "aoColumns": aoColumns,
			"fnDrawCallback": function(){
					//persistChecked();
				 },
            'fnServerData': function (sSource, aoData, fnCallback) {
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success':fnCallback
				});
            }
		
		
		});
		
		/*$('body').off('click','.productCheckBoxC1');
		$('body').on('click','.productCheckBoxC1',function(){
			refreshFilter();
			if(oTable.fnSettings().jqXHR && oTable.fnSettings().jqXHR.readystate != 4){
		        oTable.fnSettings().jqXHR.abort();
		    }
			oTable.fnDraw();
		});*/
		$('#sample_1_processing').fadeOut(4000);
		
}

function switchProduct(change,productId){
	$.ajax({
		url:HTTP_HOST+'products/changeStatus',
		type:'POST',
		data:{'status':change,'productId':productId},
		success:function(data){
			var result = $.parseJSON(data);
			if(result.success==true){
                $('.danger-distribute-product').css('display','none');
                $('.success-distribute-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);
            	loadproducts();
            }else{
                $('.success-distribute-product').css('display','none');
                $('.danger-distribute-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);
            }
		}
	});
}



function addProductInTable(id,name,quantity,unit,ppu){

	if($('#productAssigningTable:visible').length == 0 || $('#siteInCharge').val()==''){
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
		   $('.success-distribute-product').css('display','none');
    	   $('.danger-distribute-product').html('<button class="close" data-close="alert"></button>Please select site incharge first !').css('display','block');    	
		});
		
	}else{
		var html  ='<tr id="selectedProductRow_'+id+'" class="selectedProductRow">';
			html +='<td><input name="ppu[]" type="hidden" value="'+ppu+'" />'+counter+'</td>';
			html +='<td><input name="pid[]" type="hidden" value="'+id+'" />'+id+'</td>';
			html +='<td>'+name+'</td>';
			html +='<td id="product_quantity_'+id+'"><input name="apq[]" type="hidden" value="'+quantity+'" />'+quantity+' '+unit+'</td>';
			html +='<td><input name="ipq[]" type="text" class="form-control issued_product" maxlength="20" id="product_issued_'+id+'" onkeyup="assignedQuantity('+id+','+ppu+','+quantity+')" value="0"/></td>';
			html +='<td><input name="ptp[]" type="text" class="form-control issued_product_price" maxlength="20" id="product_price_'+id+'" value="0" disabled="disabled"></td>';
			html +='<td><button class="btn btn-danger" onclick="removeSelectedItems('+id+')" title="Remove this item ?" style="padding:6px 12px; margin-right:0px;"><i class="fa  fa-trash-o"></i></button></td>';
			html +='</tr>';

		if(counter==1){
			$('#assignedProductList').html(html);
		}else{
			$('#assignedProductList').append(html);
		}
		counter++;
		$('#freezeProduct_'+id).attr('disabled',true).removeClass('btn-primary').addClass('btn-danger');		
	}

}

function removeSelectedItems(id){
	$('#selectedProductRow_'+id).remove();
	$('#freezeProduct_'+id).removeAttr('disabled').removeClass('btn-danger').addClass('btn-primary');
}

function drawTableH(){
	counter=1;
	$('#productAssigningTable').css('display','block');
}


function assignedQuantity(id,ppu,totalUnit){ 

    if ($('#product_issued_'+id).val().length > 0 && $('#product_issued_'+id).val() < totalUnit) {
        var quantity = parseInt($('#product_issued_'+id).val());
        var value = parseInt(ppu);
        $('#product_issued_'+id).removeClass('btn-warning2');
        $('#product_price_'+id).val(value*quantity).removeClass('btn-warning');
        check=true;
        
    }else if($('#product_issued_'+id).val().length==0 || $('#product_issued_'+id).val().length ==''){
    	$('#product_issued_'+id).removeClass('btn-warning2');
    	$('#product_price_'+id).val('Add quantity').addClass('btn-warning');

    }else{
    	//$('#product_issued_'+id).addClass('btn-danger');
        $('#product_price_'+id).val('Quantity Exeed').addClass('btn-warning');
    }
}


function saveProductDistribution(){
	
	if($('#siteInCharge').val()==''){
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
		   $('.success-distribute-product').css('display','none');
    	   $('.danger-distribute-product').html('<button class="close" data-close="alert"></button>Please select site incharge first !').css('display','block');    	
		});
		return false;
	}


	if($('#assignedProductList tr.odd:visible').length>0 || $('#assignedProductList tr').length==0){
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
		   $('.success-distribute-product').css('display','none');
    	   $('.danger-distribute-product').html('<button class="close" data-close="alert"></button>Add at least one product for selected Site InCharge !').css('display','block');    	
		});
		return false;
		//check = false;	
	}

	$('.issued_product').each(function(){
		var thisObj = $(this);
		if($(this).val().length==0 || $(this).val().length =='' || $(this).val()==0){
			thisObj.addClass('btn-warning2');
			check = false;
		}
		
	});

	$('.issued_product_price').each(function(){
		if($(this).hasClass('btn-warning')){
			check =false;
		}
	});

	var formata = {
		'pricePerUnit':$('input[name="ppu[]"]').serializeArray(),
		'productId':$('input[name="pid[]"]').serializeArray(),
		'productQuantity':$('input[name="ipq[]"]').serializeArray(),
		'siteInChargeId':$('#siteInCharge').val()
	}
	if(check==true){
		$.ajax({
			url:HTTP_HOST+'manage_product/saveSiteInChargeProducts',
			type:'POST',
			data:formata,
			success:function(data){
				var result = $.parseJSON(data);
				if(result.success==true){
					var body = $("html, body");
					body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
						$('.danger-distribute-product').css('display','none');
	                	$('.success-distribute-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
	            	});
	                loadproducts();
	                $('.selectedProductRow').each(function(){
	                	$(this).remove();
	                });
	            }else{
	            	var body = $("html, body");
					body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
	                	$('.success-distribute-product').css('display','none');
	                	$('.danger-distribute-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
	            	});
	            }
			}
		});
	}else{
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
		   $('.success-distribute-product').css('display','none');
    	   $('.danger-distribute-product').html('<button class="close" data-close="alert"></button>Please enter valid entries in the fields !').css('display','block');    	
		});
	}
	
}




















