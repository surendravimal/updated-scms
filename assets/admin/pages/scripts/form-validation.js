var FormValidation = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    url: {
                        required: true,
                        url: true
                    },
                    
                    number: {
                        required: true,
                        number: true
                    },
                    digits: {
                        required: true,
                        digits: true
                    },
                    creditcard: {
                        required: true,
                        creditcard: true
                    },
                    occupation: {
                        minlength: 5,
                    },
                    select: {
                        required: true
                    },
                   
                    
                    select_multi: {
                        required: true,
                        minlength: 1,
                        maxlength: 3
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    Metronic.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success1.show();
                    error1.hide();
                }
            });
    }

    // validation using icons
    var handleValidation2 = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $('#add_users');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);

        $.validator.addMethod("date",function ( value, element ) {
            var bits = value.match( /([0-9]+)/gi ), str;
                if ( ! bits )
                    return this.optional(element) || false;
                str = bits[ 1 ] + '/' + bits[ 0 ] + '/' + bits[ 2 ];
                return this.optional(element) || !/Invalid|NaN/.test(new Date( str ));
            },
            "Please enter a date in the format dd/mm/yyyy"
        );

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                firstname: {
                    minlength: 3,
                    required: true
                },
                middlename: {
                },
                lastname: {
                    required:true
                },
                email: {
                    required: true,
                    email: true
                },
                
                dob:{
                    date:true,
                    minlength:10,
                    maxlength:10
                },

                gender:{
                    required:true,
                    maxlength:12
                },
                mobile:{
                    required:true,
                    number:true
                },
                
                userRole:{
                    required:true
                },
                
                address1:{
                    required:true
                },
                
                city:{
                    required:true
                },
                
                state:{
                    required:true
                },
                country:{
                    required:true
                },
                pincode:{
                    required:true,
                    number:true,
                    minlength:6,
                    maxlength:6
                },
                landline:{
                    number:true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                Metronic.scrollTo(error2, -150);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                //form[0].submit(); // submit the form
                var formdata = form2.serialize();
                doRegistration(formdata);
            }
        });
    }

    var handleValidation_update_user = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $('#form_sample_2');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);

        $.validator.addMethod("date",function ( value, element ) {
            var bits = value.match( /([0-9]+)/gi ), str;
                if ( ! bits )
                    return this.optional(element) || false;
                str = bits[ 1 ] + '/' + bits[ 0 ] + '/' + bits[ 2 ];
                return this.optional(element) || !/Invalid|NaN/.test(new Date( str ));
            },
            "Please enter a date in the format dd/mm/yyyy"
        );

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                firstname: {
                    minlength: 3,
                    required: true
                },
                middlename: {
                },
                lastname: {
                    required:true
                },
                email: {
                    required: true,
                    email: true
                },
                
                dob:{
                    date:true,
                    minlength:10,
                    maxlength:10
                },

                gender:{
                    required:true,
                    maxlength:12
                },
                mobile:{
                    required:true,
                    number:true
                },
                
                userRole:{
                    required:true
                },
                
                address1:{
                    required:true
                },
                
                city:{
                    required:true
                },
                
                state:{
                    required:true
                },
                country:{
                    required:true
                },
                pincode:{
                    required:true,
                    number:true,
                    minlength:6,
                    maxlength:6
                },
                landline:{
                    number:true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                Metronic.scrollTo(error2, -150);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                //form[0].submit(); // submit the form
                var formdata = form2.serialize();
                updateUser2(formdata);
            }
        });
    }

    var handleValidation_update_profile = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $('#profile_update_form');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);

        $.validator.addMethod("date",function ( value, element ) {
            var bits = value.match( /([0-9]+)/gi ), str;
                if ( ! bits )
                    return this.optional(element) || false;
                str = bits[ 1 ] + '/' + bits[ 0 ] + '/' + bits[ 2 ];
                return this.optional(element) || !/Invalid|NaN/.test(new Date( str ));
            },
            "Please enter a date in the format dd/mm/yyyy"
        );

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                firstname: {
                    minlength: 3,
                    required: true
                },
                middlename: {
                },
                lastname: {
                    required:true
                },
                
                
                dob:{
                    date:true,
                    minlength:10,
                    maxlength:10
                },

                gender:{
                    required:true,
                    maxlength:12
                },
                mobile:{
                    required:true,
                    number:true
                },
                
                
                address1:{
                    required:true
                },
                
                city:{
                    required:true
                },
                
                state:{
                    required:true
                },
                country:{
                    required:true
                },
                pincode:{
                    required:true,
                    number:true,
                    minlength:6,
                    maxlength:6
                },
                landline:{
                    number:true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                Metronic.scrollTo(error2, -150);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                //form[0].submit(); // submit the form
                var formdata = form2.serialize();
                updateUserProfile(formdata);
            }
        });
    }

    var handleValidation_add_product = function() {
       
        var form2 = $('#add_product_form');
        var error2 = $('.danger-add-products', form2);
        var success2 = $('.success-add-products', form2);
        
        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                
                
                majorCategory:{
                    number:true
                },
                minorCategory:{
                    number:true
                },
                miniCategory:{
                    number:true
                },

                productShortName: {
                    minlength: 3,
                    required: true
                },
                productName: {
                    required:true
                },

                measuringUnit:{
                    required:true,
                },
                
                quantity:{
                    required:true,
                    number:true
                },
                
                pricePerUnit:{
                    required:true,
                    number:true
                },
                
                totalPrice:{
                    required:true,
                    number:true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                Metronic.scrollTo(error2, -150);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                //form[0].submit(); // submit the form
                var formdata = form2.serialize();
                addProduct(formdata);
            }
        });
    }


    var handleValidation_update_product = function() {
       
        var form2 = $('#update_product_form');
        var error2 = $('.danger-update-products', form2);
        var success2 = $('.success-update-products', form2);
        
        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                
                
                majorCategory:{
                    number:true
                },
                minorCategory:{
                    number:true
                },
                miniCategory:{
                    number:true
                },

                productShortName: {
                    minlength: 3,
                    required: true
                },
                productName: {
                    required:true
                },

                measuringUnit:{
                    required:true,
                },
                
                quantity:{
                    required:true,
                    number:true
                },
                
                pricePerUnit:{
                    required:true,
                    number:true
                },
                
                totalPrice:{
                    required:true,
                    number:true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
                Metronic.scrollTo(error2, -150);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
                
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                //form[0].submit(); // submit the form
                var formdata = form2.serialize();
                updateProduct2(formdata);
            }
        });
    }

    // advance validation
    var handleValidation3 = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form3 = $('#form_sample_3');
            var error3 = $('.alert-danger', form3);
            var success3 = $('.alert-success', form3);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form3.on('submit', function() {
                for(var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },  
                    options1: {
                        required: true
                    },
                    options2: {
                        required: true
                    },
                    select2tags: {
                        required: true
                    },
                    datepicker: {
                        required: true
                    },
                    occupation: {
                        minlength: 5,
                    },
                    membership: {
                        required: true
                    },
                    service: {
                        required: true,
                        minlength: 2
                    },
                    markdown: {
                        required: true
                    },
                    editor1: {
                        required: true
                    },
                    editor2: {
                        required: true
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    membership: {
                        required: "Please select a Membership type"
                    },
                    service: {
                        required: "Please select  at least 2 types of Service",
                        minlength: jQuery.validator.format("Please select  at least {0} types of Service")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success3.hide();
                    error3.show();
                    Metronic.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success3.show();
                    error3.hide();
                    form[0].submit(); // submit the form
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form3).change(function () {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

            // initialize select2 tags
            $("#select2_tags").change(function() {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input 
            }).select2({
                tags: ["red", "green", "blue", "yellow", "pink"]
            });

            //initialize datepicker
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                autoclose: true
            });
            $('.date-picker .form-control').change(function() {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input 
            })
    }

    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            //handleWysihtml5();
            //handleValidation1();
            handleValidation2();
            //handleValidation3();

            handleValidation_add_product();
            handleValidation_update_product();
            handleValidation_update_user();
            handleValidation_update_profile();

        }

    };

}();


//add_product_form

function doRegistration(formdata){
    $.ajax({
        url:HTTP_HOST+'Users/addUser',
        data:formdata,
        type:'POST',
        success:function(data){
            var result = $.parseJSON(data);
            if(result.success==true){
                $('.alert-danger').css('display','none');
                $('.alert-success').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }else{
                $('.alert-success').css('display','none');
                $('.alert-danger').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }
        }
    });
}

function updateUser2(formdata){
    $.ajax({
        url:HTTP_HOST+'manage_user/updateUser',
        data:formdata,
        type:'POST',
        success:function(data){
            var result = $.parseJSON(data);
            if(result.success==true){
                $('.alert-danger').css('display','none');
                $('.alert-success').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
                loadUsers();
            }else{
                $('.alert-success').css('display','none');
                $('.alert-danger').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }
        }
    })

}



function updateUserProfile(formdata){
    $.ajax({
        url:HTTP_HOST+'profile/updateProfile',
        data:formdata,
        type:'POST',
        success:function(data){
            var result = $.parseJSON(data);
            if(result.success==true){
                $('.alert-danger').css('display','none');
                $('.alert-success').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
                setTimeout(function(){
                   window.location.reload();
                }, 6000);
            }else{
                $('.alert-success').css('display','none');
                $('.alert-danger').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }
        }
    })

}

function addProduct(formdata){
     $.ajax({
        url:HTTP_HOST+'Add_products/addNewProduct',
        data:formdata,
        type:'POST',
        success:function(data){
            var result = $.parseJSON(data);
            if(result.success==true){
                $('.danger-add-products').css('display','none');
                $('.success-add-products').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
                //loadUsers();
            }else{
                $('.success-add-products').css('display','none');
                $('.danger-add-products').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }
        }
    })
}

function updateProduct2(formdata){
     $.ajax({
        url:HTTP_HOST+'products/updateProduct',
        data:formdata,
        type:'POST',
        success:function(data){
            var result = $.parseJSON(data);
            if(result.success==true){
                $('.danger-update-products').css('display','none');
                $('.success-update-products').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
                loadproducts();
            }else{
                $('.success-update-products').css('display','none');
                $('.danger-update-products').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }
        }
    })
}


function reset_password(){
    var formdata = {'oldPassword':$('#oldPassword').val(),'newPassword':$('#newPassword').val(),'confirmPassword':$('#confirmPassword').val()};
    $.ajax({
        url:HTTP_HOST+'manage_user/processResetPassword',
        data:formdata,
        type:'POST',
        success:function(data){
            var result = $.parseJSON(data);
            if(result.success==true){
                $('.danger-reset-password').css('display','none');
                $('.success-reset-password').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }else{
                $('.success-reset-password').css('display','none');
                $('.danger-reset-password').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block');
            }
        }
    })    
}



$("#pricePerUnit").on('keyup blur', function (e) {
    
    if ($(this).val().length > 0) {
        var value = parseInt($(this).val());
        var quantity = parseInt($('#quantity').val());
        $('#totalPrice').val(value*quantity);
        
    }else{
        $('#totalPrice').val(0);
    }
});


$("#quantity").on('keyup blur', function (e) {
    
    if ($(this).val().length > 0) {
        var value = parseInt($(this).val());
        var quantity = parseInt($('#pricePerUnit').val());
        $('#totalPrice').val(value*quantity);
        
    }else{
        $('#totalPrice').val(0);
    }
});