// JavaScript Document
var oTable = "";
var checkedItems = new Array();


/*var persistChecked = function(){
	$('input[type="checkbox"][name="UserCheckBox"]','.datatable').each(function(){
		if (checkedItemsContains($(this).attr('id'))){
			$(this).attr('checked','checked');
		}else{
		 //$(this).removeAttr('checked');
		}
	});
}*/


$(document).ready(function(){
	loadUsers();
});

function loadUsers(){
	var aoColumns = [
				{"aaData": "S.No.", "sTitle":"S.No.","sWidth":"2%"},
				{"aaData": "hidden", "sTitle":"hidden","sWidth": "10","bSortable": false,"bVisible": false},
				{"aaData": "Name", "sTitle": "Name","bSortable": true,"sWidth":"18%"},
				{"aaData": "Email", "sTitle": "Email","bSortable": false,"sWidth":"20%"},
				{"aaData": "Phone", "sTitle": "Phone","bSortable": false,"sWidth":"10%"},
				{"aaData": "Role", "sTitle": "Role","bSortable": true,"sWidth":"10%"},
				{"aaData": "Status", "sTitle": "Status","bSortable": false,"sWidth":"12%"},
				{"aaData": "Location", "sTitle": "Location","bSortable": true,"sWidth":"10%"},
				{"aaData": "Date", "sTitle": "Date","bSortable": false,"sWidth":"12%"},
				{"aaData": "Action", "sTitle": "Action","bSortable": false,"sWidth":"10%"},
				];
	
		oTable = $('#sample_1').dataTable({
			"bProcessing": true,
            "bServerSide": true,
			"bDestroy": true,
			"sAjaxSource": HTTP_HOST+'manage_user/getUserList',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 5,
            "oLanguage": {
                "sProcessing": '<img src="'+HTTP_HOST+'assets/global/img/loader.gif" alt="Processing..">'
            },
			'bAutoWidth': false,
            "aoColumns": aoColumns,
			"fnDrawCallback": function(){
					//persistChecked();
				 },
            'fnServerData': function (sSource, aoData, fnCallback) {
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success':fnCallback
				});
            }
		
		
		});
		
		/*$('body').off('click','.UserCheckBoxC1');
		$('body').on('click','.UserCheckBoxC1',function(){
			refreshFilter();
			if(oTable.fnSettings().jqXHR && oTable.fnSettings().jqXHR.readystate != 4){
		        oTable.fnSettings().jqXHR.abort();
		    }
			oTable.fnDraw();
		});*/
		$('#sample_1_processing').fadeOut(4000);
		
}


function updateUser(userId,type){
	$.ajax({
		url:HTTP_HOST+'manage_user/updateUser',
		type:'POST',
		data:{'type':type,'userId':userId},
		success:function(data){
			var result = $.parseJSON(data);
			if(result.success==true && type=='get'){
				$('#userId').val(result.data.userId);
				$('#email').val(result.data.emailId);
				$('#mobile').val(result.data.mobile);
				$('#landline').val(result.data.officeLandLine);
				$('#privilege option[value="'+result.data.privilege+'"]').attr('selected',true);
				$('#userToken').val(result.data.userToken);
				$('#firstname').val(result.data.firstName);
				$('#middlename').val(result.data.middleName);
				$('#lastname').val(result.data.lastName);
				$('#gender option[value="'+result.data.gender+'"]').attr('selected',true);
				$('#profile').val(result.data.profile); //select 
				
				$('#address1').val(result.data.address1);
				$('#address2').val(result.data.address2);
				$('#city').val(result.data.city);
				$('#state').val(result.data.state);
				$('#country option[value="'+result.data.country+'"]').attr('selected',true);
				$('#pincode').val(result.data.pincode);
				$('#dob').val(result.data.dob);
				$('#portlet-config').modal('show');
			}else{
				alert('No Record Found !!')
			}
		}
	});
	
}

function removeUser(name,userId){
	$('.success-remove-user').css('display','none');
    $('.danger-remove-user').html('<button class="close" data-close="alert"></button>Do you really want to remove <b>'+name+'</b> from user list ?   <button class="label label-sm label-success" id="removeConfirm" value="Yes">Yes</button><button class="label label-sm label-danger" id="removeCancel" value="No">No</button>').css('display','block');

    $('#removeConfirm').click(function(){
    	$.ajax({
			url:HTTP_HOST+'manage_user/removeUser',
			type:'POST',
			data:{'userId':userId},
			success:function(data){
				var result = $.parseJSON(data);
				if(result.success==true){
	                $('.danger-remove-user').css('display','none');
	                $('.success-remove-user').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(10000);
	            	loadUsers();
	            }else{
	                $('.success-remove-user').css('display','none');
	                $('.danger-remove-user').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(10000);
	            }
			}
		});
    });

    $('#removeCancel').click(function(){
    	$('.danger-remove-user').fadeOut(1000);//css('display','none');
    });

    
}










