$(function() {
   refreshMajorCate();
   //refreshMinorCate2();
});
//minorCateIdForMini
 //refreshMajorCate();



function major_category(value){
	refreshMinorCate(value);
}


function minor_category(){
	var value0 = $('#majorCategory').val();
	var value = $('#minorCategory').val();
	refreshMiniCate(value0,value);
}


function addMajorCategory(){
	var category = $('#majorCateName').val();
	var formdata = {'majorCateName':category};
	$.ajax({
		'url':HTTP_HOST+'Add_products/addMajorCategory',
		'type':'POST',
		'data':formdata,
		'success':function(data){
			var result = $.parseJSON(data);
			if(result.success==true){
                $('.danger-major-category').css('display','none');
                $('.success-major-category').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);;
            	refreshMajorCate();
            }else{
                $('.success-major-category').css('display','none');
                $('.danger-major-category').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);;
            }
		}
	});
}


function addMinorCategory(){
	var majorCategoryId = $('#majorCateIdForMinor').val();
	if(majorCategoryId !=''){
		var category = $('#minorCateName').val();
		var formdata = {'minorCateName':category,'majorCateId':majorCategoryId};
		$.ajax({
			'url':HTTP_HOST+'Add_products/addMinorCategory',
			'type':'POST',
			'data':formdata,
			'success':function(data){
				var result = $.parseJSON(data);
				if(result.success==true){
	                $('.danger-minor-category').css('display','none');
	                $('.success-minor-category').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);
	            	refreshMajorCate();
	            	//refreshMinorCate2();
	            }else{
	                $('.success-minor-category').css('display','none');
	                $('.danger-minor-category').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);;
	            }
			}
		});
	}else{
		$('.success-minor-category').css('display','none');
	    $('.danger-minor-category').html('<button class="close" data-close="alert"></button>Please select main category first!').css('display','block').fadeOut(5000);;
	}
	
}


function addMiniCategory(){
	var majorCategoryId = $('#majorCateIdForMini').val();
	var minorCategoryId = $('#minorCateIdForMini').val();
	var category = $('#miniCateName').val();
	if(majorCategoryId !=''){
		if(minorCategoryId !=''){
			var formdata = {'miniCateName':category,'majorCateId':majorCategoryId,'minorCateId':minorCategoryId};
			$.ajax({
				'url':HTTP_HOST+'Add_products/addMiniCategory',
				'type':'POST',
				'data':formdata,
				'success':function(data){
					var result = $.parseJSON(data);
					if(result.success==true){
		                $('.danger-mini-category').css('display','none');
		                $('.success-mini-category').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);
		            	refreshMajorCate();
		            }else{
		                $('.success-mini-category').css('display','none');
		                $('.danger-mini-category').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(5000);
		            }
				}
			});
		}else{
			$('.success-mini-category').css('display','none');
	    	$('.danger-mini-category').html('<button class="close" data-close="alert"></button>Please select sub category first!').css('display','block').fadeOut(5000);
		}
	}else{
		$('.success-mini-category').css('display','none');
	    $('.danger-mini-category').html('<button class="close" data-close="alert"></button>Please select main category first!').css('display','block').fadeOut(5000);
	}
	
}


function refreshMajorCate(){
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMajorCate',
		'type':'POST',
		'success':function(data){
			var result = $.parseJSON(data);
			if(result.success==true){
				$("#majorCategory").select2("val", "");
				var html ='<option value="">Select...</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].majorCateId+'">'+result.data[i].majorCateName+'</option>'
				}
				$('#majorCategory').html(html);
				$('#majorCateIdForMinor').html(html);
				$('#majorCateIdForMini').html(html);
				
            }else{
            
            }
		}
	});
}

function refreshMinorCate(value){
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMinorCate',
		'type':'POST',
		'data':{'majorCateId':value},
		'success':function(data){
			var result = $.parseJSON(data);
			
			if(result.success==true){
				$('select[name="minorCategory"]').removeAttr('disabled');
				var html ='<option value="">Select..</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].minorCateId+'">'+result.data[i].minorCateName+'</option>'
				}
				$('#minorCategory').html(html);
				$('#minorCateIdForMini').html(html);

            }else{
            	$('#minorCategory').attr('disabled',true);	
				$('#minorCategory').html('<option value="">No Category Found</option>');
            }

            $("#minorCategory").select2("val", "");
		}
	});
}

function refreshMiniCate(value0,value){
	
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMiniCate',
		'type':'POST',
		'data':{'majorCateId':value0,'minorCateId':value},
		'success':function(data){
			var result = $.parseJSON(data);
			
			if(result.success==true){
				$('select[name="miniCategory"]').removeAttr('disabled');
				var html ='<option value="">Select..</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].miniCateId+'">'+result.data[i].miniCateName+'</option>'
				}
				$('#miniCategory').html(html);
            }else{
            	$('select[name="miniCategory"]').attr('disabled',true);	
				$('select[name="miniCategory"]').html('<option value="">No Category Found</option>');
            }
            $("#miniCategory").select2("val", "");
		}
	});
}