// JavaScript Document
var oTable = "";
var checkedItems = new Array();


/*var persistChecked = function(){
	$('input[type="checkbox"][name="productCheckBox"]','.datatable').each(function(){
		if (checkedItemsContains($(this).attr('id'))){
			$(this).attr('checked','checked');
		}else{
		 //$(this).removeAttr('checked');
		}
	});
}*/


$(document).ready(function(){
	loadproducts();
});

function loadproducts(){
	var aoColumns = [
				{"aaData": "#", "sTitle":"#","sWidth":"1%"},
				{"aaData": "hidden", "sTitle":"hidden","sWidth": "10","bSortable": false,"bVisible": false},
				{"aaData": "Name", "sTitle": "Name","bSortable": true,"sWidth":"14%"},
				{"aaData": "Category1", "sTitle": "Category1","bSortable": true,"sWidth":"12%"},
				{"aaData": "Category2", "sTitle": "Category2","bSortable": false,"sWidth":"10%"},
				{"aaData": "Category3", "sTitle": "Category3","bSortable": false,"sWidth":"10%"},
				{"aaData": "Status", "sTitle": "Status","bSortable": false,"sWidth":"8%"},
				{"aaData": "Quantity", "sTitle": "Quantity","bSortable": false,"sWidth":"10%"},
				{"aaData": "Total Price", "sTitle": "Total Price","bSortable": false,"sWidth":"8%"},
				{"aaData": "Date", "sTitle": "Date","bSortable": false,"sWidth":"11%"},
				{"aaData": "Add By", "sTitle": "Add By","bSortable": true,"sWidth":"10%"},
				{"aaData": "Action", "sTitle": "Action","bSortable": false,"sWidth":"8%"},
				];
	
		oTable = $('#sample_1').dataTable({
			"bProcessing": true,
            "bServerSide": true,
			"bDestroy": true,
			"sAjaxSource": HTTP_HOST+'products/getproductList',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 5,
            "oLanguage": {
                "sProcessing": '<img src="'+HTTP_HOST+'assets/global/img/loader.gif" alt="Processing..">'
            },
			'bAutoWidth': false,
            "aoColumns": aoColumns,
			"fnDrawCallback": function(){
					//persistChecked();
				 },
            'fnServerData': function (sSource, aoData, fnCallback) {
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success':fnCallback
				});
            }
		
		
		});
		
		/*$('body').off('click','.productCheckBoxC1');
		$('body').on('click','.productCheckBoxC1',function(){
			refreshFilter();
			if(oTable.fnSettings().jqXHR && oTable.fnSettings().jqXHR.readystate != 4){
		        oTable.fnSettings().jqXHR.abort();
		    }
			oTable.fnDraw();
		});*/
		$('#sample_1_processing').fadeOut(4000);
		
}


function updateProduct(productId,type){
	var val1='';
	var val2='';
	$.ajax({
		url:HTTP_HOST+'products/updateproduct',
		type:'POST',
		data:{'type':type,'productId':productId},
		async:false,
		success:function(data){
			var result = $.parseJSON(data);
			if(result.success==true && type=='get'){
				$('#productId').val(result.data.productId);
				$('#productName').val(result.data.productName);
				$('#productShortName').val(result.data.productShortName);
				$("#majorCategory").select2().select2("val", result.data.majorCateId);
				refreshMinorCate2(result.data.majorCateId,result.data.minorCateId);
				
				refreshMiniCate2(result.data.majorCateId,result.data.minorCateId,result.data.miniCateId);
				
				$("#measuringUnit").select2().select2("val", result.data.measuringUnit);
				$('#quantity').val(result.data.quantity);
				$('#pricePerUnit').val(result.data.pricePerUnit);
				$('#totalPrice').val(result.data.totalPrice);
				$('#lastname').val(result.data.lastName);
				
				$('#portlet-config').modal('show');
			}else{
				alert('No Record Found !!')
			}
		}
	});
	$("#minorCategory").select2().select2("val", val2);
	
}




function refreshMinorCate2(value,value2){
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMinorCate',
		'type':'POST',
		'data':{'majorCateId':value},
		'success':function(data){
			var result = $.parseJSON(data);
			$("#minorCategory").select2("val", "");
			if(result.success==true){
				$('select[name="minorCategory"]').removeAttr('disabled');
				var html ='<option value="">Select..</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].minorCateId+'">'+result.data[i].minorCateName+'</option>'
				}
				$('#minorCategory').html(html);
				$('#minorCateIdForMini').html(html);
				$("#minorCategory").select2().select2("val", value2);

            }else{
            	$('select[name="minorCategory"]').attr('disabled',true);	
				$('select[name="minorCategory"]').html('<option value="">No Category Found</option>');
            }
		}
	});
}

function refreshMiniCate2(value0,value,value2){
	
	$.ajax({
		'url':HTTP_HOST+'Add_products/getMiniCate',
		'type':'POST',
		'data':{'majorCateId':value0,'minorCateId':value},
		'success':function(data){
			var result = $.parseJSON(data);
			$("#miniCategory").select2("val", "");
			if(result.success==true){
				$('select[name="miniCategory"]').removeAttr('disabled');
				var html ='<option value="">Select..</option>';
				for(var i = 0; i<result.data.length;i++){
					html +='<option value="'+result.data[i].miniCateId+'">'+result.data[i].miniCateName+'</option>'
				}
				$('#miniCategory').html(html);
				$("#miniCategory").select2().select2("val", value2);

            }else{
            	$('select[name="miniCategory"]').attr('disabled',true);	
				$('select[name="miniCategory"]').html('<option value="">No Category Found</option>');
            }
		}
	});
}

function removeProduct(name,productId){
	$('.success-remove-product').css('display','none');
    $('.danger-remove-product').html('<button class="close" data-close="alert"></button>Do you really want to remove <b>'+name+'</b> from product list ?   <button class="label label-sm label-success" id="removeConfirm" value="Yes">Yes</button><button class="label label-sm label-danger" id="removeCancel" value="No">No</button>').css('display','block');

    $('#removeConfirm').click(function(){
    	$.ajax({
			url:HTTP_HOST+'products/removeproduct',
			type:'POST',
			data:{'productId':productId},
			success:function(data){
				var result = $.parseJSON(data);
				if(result.success==true){
	                $('.danger-remove-product').css('display','none');
	                $('.success-remove-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(10000);
	            	loadproducts();
	            }else{
	                $('.success-remove-product').css('display','none');
	                $('.danger-remove-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(10000);
	            }
			}
		});
    });

    $('#removeCancel').click(function(){
    	$('.danger-remove-product').fadeOut(1000);//css('display','none');
    });

    
}


function switchProduct(change,productId){
	$.ajax({
		url:HTTP_HOST+'products/changeStatus',
		type:'POST',
		data:{'status':change,'productId':productId},
		success:function(data){
			var result = $.parseJSON(data);
			if(result.success==true){
                $('.danger-remove-product').css('display','none');
                $('.success-remove-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(1000);
            	loadproducts();
            }else{
                $('.success-remove-product').css('display','none');
                $('.danger-remove-product').html('<button class="close" data-close="alert"></button>'+result.text).css('display','block').fadeOut(1000);
            }
		}
	});
}










