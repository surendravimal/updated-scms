var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                username: {
	                    required: "Username is required."
	                },
	                password: {
	                    required: "Password is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-danger', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                //form.submit();
	                do_login();
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    //$('.login-form').submit();
	                	do_login();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                //form.submit();
	                forget_password();
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    //$('.forget-form').submit();
	                    forget_password();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            
        }

    };

}();


function do_login(){
	$("#response").css('display','block');
	$("#loader").css('display','block');
	$("#submit_button").attr('disabled','true').css({'background-color':'#1a6ef6','color':'#FFF','opacity':.6});
	var formData =$('#login_form').serialize();
	var url =HTTP_HOST+"ajax_request/process_login";
	$.ajax({
		  type: "POST",
		  url: url,
		  data: formData,
		  success: function(data){
			var result = $.parseJSON(data);
			if(result.success==true){
				$("#loader").css('display','none');
				$("#response").text(result.text).css('color','green').fadeOut(5000,function(){ $("#response").text('Please wait..');});
				$("#submit_button").removeAttr('disabled').css({'background-color':'#4b8df8','color':'#FFFFFF','opacity':1});
				
				window.location.href=HTTP_HOST+'welcome';
			}else{
				$("#loader").css('display','none');
				$(".display-hide").css('display','block');
				$("#response").css('display','none').text('Please wait..');;
				$(".display-hide span").text(result.text).css({'display':'block'});
				$("#submit_button").removeAttr('disabled').css({'background-color':'#4b8df8','color':'#FFFFFF','opacity':1});	
				
			}
		  }
	});
}


function forget_password(){
	$("#response2").css('display','block');
	$("#loader2").css('display','block');
	$("#forget-btn").attr('disabled','true').css({'background-color':'#1a6ef6','color':'#FFF','opacity':.6});
	var formData =$('#forget-form').serialize();
	var url =HTTP_HOST+"ajax_request/process_forget_password";
	$.ajax({
		  type: "POST",
		  url: url,
		  data: formData,
		  success: function(data){
			var result = $.parseJSON(data);
			if(result.success==true){
				$("#loader2").css('display','none');
				$("#response2").text(result.text).css('color','white').fadeOut(10000,function(){ $("#response2").text('Please wait..');});
				$("#forget-btn").removeAttr('disabled').css({'background-color':'#4b8df8','color':'#FFFFFF','opacity':1});
				
			}else{
				$("#loader2").css('display','none');
				$(".display-hide2").css('display','block');
				$("#response2").css('display','none').text('Please wait..');;
				$(".display-hide2 span").text(result.text).css({'display':'block'});
				$("#forget-btn").removeAttr('disabled').css({'background-color':'#4b8df8','color':'#FFFFFF','opacity':1});	
				
			}
		  }
	});
}