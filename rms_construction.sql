-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 26, 2018 at 07:37 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rms_construction`
--

DELIMITER $$
--
-- Functions
--
$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `major_category`
--

CREATE TABLE IF NOT EXISTS `major_category` (
  `majorCateId` int(11) NOT NULL AUTO_INCREMENT,
  `majorCateName` varchar(100) NOT NULL,
  `superCateShortName` varchar(45) NOT NULL DEFAULT 'Electrical',
  PRIMARY KEY (`majorCateId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `major_category`
--

INSERT INTO `major_category` (`majorCateId`, `majorCateName`, `superCateShortName`) VALUES
(2, 'Perishable ', 'Food'),
(3, 'None Perishable', 'Food'),
(4, 'Fire Fighting', 'Electrical'),
(5, 'Fire Alarm System', 'Electrical'),
(6, 'CCTV', 'Electrical'),
(7, 'Telephone', 'Electrical'),
(8, 'Aggrigate', 'CIVIL'),
(9, 'Sand', 'CIVIL'),
(10, 'Bricks', 'CIVIL'),
(11, 'Cement', 'CIVIL'),
(12, 'Steel Bar', 'CIVIL'),
(13, 'RMC', 'CIVIL'),
(14, 'Pramod', 'Electrical');

-- --------------------------------------------------------

--
-- Table structure for table `mini_category`
--

CREATE TABLE IF NOT EXISTS `mini_category` (
  `miniCateId` int(11) NOT NULL AUTO_INCREMENT,
  `miniCateName` varchar(100) NOT NULL,
  `minorCateId` int(11) NOT NULL,
  `majorCateId` int(11) NOT NULL,
  PRIMARY KEY (`miniCateId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=246 ;

--
-- Dumping data for table `mini_category`
--

INSERT INTO `mini_category` (`miniCateId`, `miniCateName`, `minorCateId`, `majorCateId`) VALUES
(36, '', 7, 1),
(38, '', 7, 1),
(39, '3 CORE 70 SQ. MM', 7, 1),
(40, '3 CORE 95 SQ. MM', 7, 1),
(41, '3 CORE 120 SQ. MM', 7, 1),
(42, '', 7, 1),
(43, '', 7, 1),
(44, '', 7, 1),
(45, '3 CORE 300 SQ. MM', 7, 1),
(46, '3 CORE 400 SQ. MM', 7, 1),
(50, '3.5 CORE 70 SQ. MM.', 8, 1),
(51, '3.5 CORE 95 SQ. MM.', 8, 1),
(52, '', 8, 1),
(53, '3.5 CORE 150 SQ. MM.', 8, 1),
(54, '3.5 CORE 185 SQ. MM.', 8, 1),
(55, '3.5 CORE 240 SQ. MM.', 8, 1),
(56, '3.5 CORE 300 SQ. MM.', 8, 1),
(57, '3.5 CORE 500 SQ. MM.', 8, 1),
(58, '3.5 CORE 630 SQ. MM.', 8, 1),
(59, '3.5 CORE 800 SQ. MM.', 8, 1),
(60, '3.5 CORE 1000 SQ. MM.', 8, 1),
(73, '13 NO', 10, 1),
(74, '40x6 MM', 19, 1),
(75, '50x6 MM', 19, 1),
(76, '25x6 MM', 19, 1),
(77, '25x5 MM', 19, 1),
(78, 'GI EARTH PIPE 4.5 MM', 13, 1),
(79, 'GI EARTH PLATE (600X600X6)MM', 13, 1),
(80, 'COPPER EARTH PLATE (600X600X3)MM', 13, 1),
(81, '25x5 MM', 20, 1),
(82, '25x6 MM', 20, 1),
(83, '40x6 MM', 20, 1),
(84, '50x6 MM', 20, 1),
(85, '38x6 MM', 19, 1),
(86, '25x3 MM', 19, 1),
(87, '25x3 MM', 20, 1),
(88, '38x6 MM', 20, 1),
(89, '2X16SQ.MM(STRAIGHT JOINT)', 30, 1),
(90, '2X25SQ.MM(STRAIGHT JOINT)', 30, 1),
(91, '2X35 SQ.MM(STRAIGHT JOINT)', 30, 1),
(92, '3.5X25 SQ.MM(STRAIGHT JOINT)', 30, 1),
(93, '3.5X35 SQ.MM(STRAIGHT JOINT)', 30, 1),
(94, '3.5X50 SQ.MM(STRAIGHT JOINT)', 30, 1),
(95, '3.5X70 SQ.MM(STRAIGHT JOINT)', 30, 1),
(96, '3.5X95 SQ.MM(STRAIGHT JOINT)', 30, 1),
(97, '3.5X120 SQ.MM(STRAIGHT JOINT)', 30, 1),
(98, '3.5X150 SQ.MM(STRAIGHT JOINT)', 30, 1),
(99, '3.5X185 SQ.MM(STRAIGHT JOINT)', 30, 1),
(101, '3.5 CORE 225 SQ. MM', 30, 1),
(102, '3.5 CORE 240 SQ. MM', 30, 1),
(103, '3.5 CORE 300 SQ. MM', 30, 1),
(104, '3.5 CORE 400 SQ. MM', 30, 1),
(105, '4 CORE 16 SQ. MM', 30, 1),
(106, '4 CORE 25 SQ. MM', 30, 1),
(107, '4 CORE 35 SQ. MM. (STRAIGHT JOINT)', 30, 1),
(108, '4 CORE 50 SQ. MM. (STRAIGHT JOINT)', 30, 1),
(109, '3 CORE 70 SQ. MM. (INDOOR END)', 31, 1),
(110, '3 CORE 120 SQ. MM(INDOOR END)', 31, 1),
(111, '3 CORE 185 SQ. MM(INDOOR END)', 31, 1),
(112, '3 CORE 240 SQ. MM(INDOOR END)', 31, 1),
(113, '3 CORE 300 SQ. MM(INDOOR END)', 31, 1),
(114, '3 CORE 70 SQ. MM(OUTDOOR END)', 31, 1),
(115, '3 CORE 120 SQ. MM(OUTDOOR END)', 31, 1),
(116, '3 CORE 240 SQ. MM(OUTDOOR END)', 31, 1),
(117, '3 CORE 300 SQ. MM(OUTDOOR END)', 31, 1),
(118, '3 CORE 185 SQ. MM(OUTDOOR END)', 31, 1),
(119, '3 CORE 70 SQ. MM(STRAIGHT JOINT)', 31, 1),
(120, '3 CORE 120 SQ. MM(STRAIGHT JOINT)', 31, 1),
(121, '3 CORE 185 SQ. MM(STRAIGHT JOINT)', 31, 1),
(122, '3 CORE 240 SQ. MM(STRAIGHT JOINT)', 31, 1),
(123, '3 CORE 300 SQ. MM(STRAIGHT JOINT)', 31, 1),
(124, '75MMX50MMX1.6MM', 28, 1),
(125, '100MMX50MMX1.6MM', 28, 1),
(126, '150MMX50MMX1.6MM', 28, 1),
(127, '225MMX50MMX1.6MM', 28, 1),
(128, '300MMX50MMX1.6MM', 28, 1),
(129, '375MMX50MMX1.6MM', 28, 1),
(130, '450MMX50MMX1.6MM', 28, 1),
(131, '6000MMX50MMX2MM', 28, 1),
(132, '300MMX62.5MMX2MM', 28, 1),
(133, '375MMX62.5MMX2MM', 28, 1),
(134, '450MMX62.5MMX2MM', 28, 1),
(135, '600MMX62.5MMX2MM', 28, 1),
(136, '750MMX62.5MMX2MM', 28, 1),
(137, '900MMX62.5MMX2MM', 28, 1),
(138, '50 MM DIA', 27, 1),
(139, '75 MM DIA', 27, 1),
(140, '110 MM DIA', 27, 1),
(141, '125 MM DIA', 27, 1),
(142, '150 MM DIA', 27, 1),
(143, '100 MM DIA', 26, 1),
(157, 'FOR 6 SQ. MM ', 11, 1),
(158, 'FOR 10 SQ. MM ', 11, 1),
(159, 'FOR 16 SQ. MM ', 11, 1),
(160, 'FOR 25 SQ. MM ', 11, 1),
(161, 'FOR 35 SQ. MM ', 11, 1),
(162, 'FOR 50 SQ. MM ', 11, 1),
(164, 'FOR 95 SQ. MM ', 11, 1),
(167, 'FOR 185 SQ. MM ', 11, 1),
(168, 'FOR 240 SQ. MM ', 11, 1),
(169, 'FOR 300 SQ. MM ', 11, 1),
(170, 'FOR 400 SQ. MM ', 11, 1),
(182, '25 MM DIA', 37, 2),
(183, '20 MM DIA', 37, 2),
(184, '32 MM DIA', 37, 2),
(186, '50 MM DIA', 37, 2),
(187, '20 MM DIA', 38, 2),
(188, '25 MM DIA', 38, 2),
(189, '32 MM DIA', 38, 2),
(190, '40 MM DIA', 38, 2),
(191, '2 MODULE', 45, 2),
(192, '3 MODULE', 45, 2),
(193, '4 MODULE', 45, 2),
(194, '6 MODULE', 45, 2),
(195, '8 MODULE', 45, 2),
(196, '12 MODULE', 45, 2),
(197, '100 AMP', 54, 2),
(198, '125 AMP', 54, 2),
(199, '150 AMP', 54, 2),
(200, '200 AMP', 54, 2),
(201, '250 AMP', 54, 2),
(202, '315 AMP', 54, 2),
(203, '400 AMP', 54, 2),
(204, '500 AMP', 54, 2),
(205, '630 AMP', 54, 2),
(206, '800 AMP', 54, 2),
(207, '100 AMP', 55, 2),
(208, '125 AMP', 55, 2),
(209, '200 AMP', 55, 2),
(210, '250 AMP', 55, 2),
(211, '400 AMP', 55, 2),
(212, '630 AMP', 55, 2),
(213, '100 pair', 33, 2),
(214, '20 pair', 33, 2),
(215, '80 pair', 33, 2),
(216, '1 PAIR', 29, 2),
(217, '4 PAIR', 29, 2),
(218, 'RG-6', 80, 2),
(219, 'CAT 6 LAN CABLE', 81, 2),
(220, 'AC SOCKET', 82, 2),
(221, '25 mm wall 1W', 83, 2),
(222, '25 mm wall 2W', 83, 2),
(223, '25 mm wall 3W', 83, 2),
(224, '25 mm wall 4W', 83, 2),
(225, '20 mm wall 4W', 83, 2),
(226, '25 mm pvc bend', 41, 2),
(227, '20 mm pvc bend', 41, 2),
(228, 'RED', 84, 2),
(229, 'YELLOW', 84, 2),
(230, 'BLUE', 84, 2),
(231, 'BLACK', 84, 2),
(232, 'GREEN', 84, 2),
(233, '32 mm PVC BEND', 41, 2),
(234, '5 Panel Board', 5, 1),
(235, 'G.I PIPE EARTHING ', 85, 2),
(236, 'G.I PLATE EARTHING ', 85, 2),
(237, 'COPPER PLATE EARTHING ', 85, 2),
(238, 'COPPER PIPE EARTHING ', 85, 2),
(239, 'Electronics type', 52, 2),
(240, '3 MODULE', 91, 2),
(241, '1/2 MODULE', 91, 2),
(242, '20mm pvc socket', 43, 2),
(243, '25mm pvc socket', 43, 2),
(244, '6 amp switch', 94, 2);

-- --------------------------------------------------------

--
-- Table structure for table `minor_category`
--

CREATE TABLE IF NOT EXISTS `minor_category` (
  `minorCateId` int(11) NOT NULL AUTO_INCREMENT,
  `minorCateName` varchar(100) NOT NULL,
  `majorCateId` int(11) NOT NULL,
  PRIMARY KEY (`minorCateId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `minor_category`
--

INSERT INTO `minor_category` (`minorCateId`, `minorCateName`, `majorCateId`) VALUES
(3, 'Vegetables', 1),
(4, 'Fruits', 1),
(5, 'HT PANEL', 1),
(6, 'LT PANEL', 1),
(7, 'HT cable', 1),
(8, 'Dry Fruits', 1),
(9, 'Control cable', 1),
(10, 'Gland', 1),
(11, 'Thimble', 1),
(12, 'Tie', 1),
(13, 'Earthing', 1),
(14, 'Bus Duct', 1),
(15, 'DMC Insulator', 1),
(16, 'Sleave', 1),
(17, 'Rubber Mat', 1),
(18, 'Route Marker', 1),
(19, 'Copper Strip', 1),
(20, 'G.I Strip', 1),
(21, 'Fire Extinguishers', 1),
(22, 'Pin Insulator', 1),
(23, 'Disc Insulator', 1),
(24, 'ACSR DOG Conductor', 1),
(25, 'SWG Wire', 1),
(26, 'Hume Pipe', 1),
(27, 'G.I Pipe', 1),
(28, 'Cable Tray', 1),
(29, 'telephone wire', 2),
(30, 'LT JOINT KIT', 1),
(31, 'HT JOINT KIT', 1),
(33, 'TAG BLOCK', 2),
(34, 'TELEPHONE SOCKET', 2),
(35, 'TV SOCKET', 2),
(36, 'DATA SOCKET', 2),
(37, 'PVC CONDUIT', 2),
(38, 'G.I CONDUIT', 2),
(39, 'FAN BOX', 2),
(40, 'FRLS PVC INSULATED COPPER WIRE', 2),
(41, 'PVC BEND', 2),
(42, 'GI BEND', 2),
(43, 'PVC SOCKET', 2),
(44, 'GI SOCKET', 2),
(45, 'MODULAR GI BOX', 2),
(46, 'CEILING ROSE', 2),
(47, 'SINGLE POLE 5/6 AMP ONE WAY MODULAR SWITCH', 2),
(48, '15/16 AMP MODULAR SWITCH', 2),
(49, '3 PIN 5/6 AMP MODULAR SOCKET OUTLET', 2),
(50, '6 PIN 5/6 AMP MODULAR SOCKET OUTLET', 2),
(51, 'MODULAR BELL PUSH SWITCH', 2),
(52, 'MODULAR FAN REGULATOR', 2),
(53, 'MODULAR BLANKING PLATE', 2),
(54, '3 POLE MCCB', 2),
(55, '4 POLE MCCB', 2),
(56, '6 to 32 AMP SP MCB', 2),
(57, '6 to 32 AMP SPN MCB', 2),
(58, '6 to 32 AMP DP MCB', 2),
(59, '6 to 32 AMP TP MCB', 2),
(60, '6 to 32 AMP TPN MCB', 2),
(61, '25 AMP 2 POLE RCCB', 2),
(62, '40 AMP 2 POLE RCCB', 2),
(63, '63 AMP 2 POLE RCCB', 2),
(64, '25 AMP 4 POLE RCCB', 2),
(65, '40 AMP 4 POLE RCCB', 2),
(66, '63 AMP 4 POLE RCCB', 2),
(67, '6 WAY SPN DOUBLE DOOR MCB DB', 2),
(68, '12 WAY SPN DOUBLE DOOR MCB DB', 2),
(69, '8 WAY SPN DOUBLE DOOR MCB DB', 2),
(70, '14 WAY SPN DOUBLE DOOR MCB DB', 2),
(71, '4 WAY TPN DOUBLE DOOR MCB DB', 2),
(72, '4 WAY TPN DOUBLE DOOR MCB DB(HORIZONTAL)', 2),
(73, '6 WAY TPN DOUBLE DOOR MCB DB(HORIZONTAL)', 2),
(74, '8 WAY TPN DOUBLE DOOR MCB DB(HORIZONTAL)', 2),
(75, '4 WAY TPN DOUBLE DOOR MCB DB(VERTICAL)', 2),
(77, '6 WAY TPN DOUBLE DOOR MCB DB(VERTICAL)', 2),
(78, '8 WAY TPN DOUBLE DOOR MCB DB(VERTICAL)', 2),
(79, '12 WAY TPN DOUBLE DOOR MCB DB(VERTICAL)', 2),
(80, 'TV WIRE', 2),
(81, 'DATA WIRE', 2),
(82, '6 PIN 25 AMP ', 2),
(83, 'junction box', 2),
(84, 'PVC Tape', 2),
(85, 'Earthing', 2),
(86, '40mm', 8),
(87, 'Steel wire', 2),
(88, 'C.I Haudi', 2),
(89, 'G.I PLATE', 2),
(90, 'JUNCTION BOX LEAD & SCREW', 2),
(91, 'Flush box', 2),
(92, 'ETPN 12 W', 2),
(93, 'SPN 8 W', 2),
(94, '6 Amp switch', 2),
(95, '16 Amp modular socket outlet', 2),
(96, 'Buzzer', 2),
(97, '1 Modular switch plate', 2),
(98, '2 Modular switch plate', 2),
(99, '3 Modular switch plate', 2),
(100, '6 Modular switch plate', 2),
(101, '8 Modular switch plate', 2),
(102, 'Exhaust Fan Louver(12")', 2),
(103, '12" Exhaust Fan', 2),
(104, 'LED Elite Green 18 Watt 6K', 2),
(105, '20 Amp AC SP DB', 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `productId` int(11) NOT NULL AUTO_INCREMENT,
  `addedBy` int(11) DEFAULT NULL,
  `productName` varchar(100) NOT NULL,
  `productShortName` varchar(45) NOT NULL,
  `majorCateId` int(11) NOT NULL,
  `minorCateId` int(11) DEFAULT NULL,
  `miniCateId` int(11) DEFAULT NULL,
  `measuringUnit` varchar(45) NOT NULL,
  `quantity` varchar(45) NOT NULL,
  `pricePerUnit` varchar(45) NOT NULL,
  `totalPrice` varchar(45) NOT NULL,
  `insertDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime NOT NULL,
  `productDescription` varchar(200) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `delete_flag` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`productId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productId`, `addedBy`, `productName`, `productShortName`, `majorCateId`, `minorCateId`, `miniCateId`, `measuringUnit`, `quantity`, `pricePerUnit`, `totalPrice`, `insertDate`, `updateDate`, `productDescription`, `status`, `delete_flag`) VALUES
(2, 1008, 'HT Cable 2.5mm', 'HTCable', 1, 2, 14, 'mtrs', '102', '16', '1632', '2016-12-08 02:52:57', '2016-12-16 00:25:53', 'Product Description !!', 'Active', 0),
(3, 1008, 'Transformer', 'T5KVA', 1, 4, 9, 'set', '4', '24000', '96000', '2016-12-08 05:05:17', '2016-12-22 15:37:14', 'Product Description !!', 'Active', 0),
(4, 1010, 'ROMA THIMBLE', 'THIM', 1, 11, NULL, 'set', '14', '120', '1680', '2016-12-16 15:58:46', '2016-12-16 21:28:46', 'Product Description !!', 'InActive', 0),
(5, 1000, 'TELEPHONE', 'teliWire', 2, 29, 0, 'mtrs', '100', '180', '18000', '2016-12-22 04:31:26', '2017-02-15 15:55:06', 'Product Description !!', 'Active', 0),
(6, 1008, 'HTCALBKLE', 'HTC', 1, 2, 14, 'sqmtrs', '190', '12', '2280', '2016-12-22 04:32:45', '2016-12-22 10:02:45', 'Product Description !!', 'Active', 0),
(7, 1008, '25 mm pvc conduit (medium)', '25 mm pvc conduit pipe(medium)', 2, 37, 182, 'mtrs', '13500', '20.88', '281880', '2017-03-29 09:42:25', '2017-07-06 13:02:51', 'Product Description !!', 'Active', 1),
(8, 1008, '25 mm pvc conduit (Heavy)', '25 mm pvc conduit pipe(Heavy)', 2, 37, 182, 'mtrs', '3000', '27', '81000', '2017-03-29 09:49:34', '2017-07-06 13:05:31', 'Product Description !!', 'Active', 1),
(9, 1008, '20 mm pvc conduit(medium)', '20 mm pvc conduit(medium)', 2, 37, 183, 'mtrs', '3000', '15.08', '45240', '2017-03-29 09:52:27', '2017-07-05 11:12:50', 'Product Description !!', 'Active', 1),
(10, 1008, '20 mm pvc conduit(Heavy)', '20 mm pvc conduit(medium)', 2, 37, 183, 'mtrs', '600', '15.08', '9048', '2017-03-29 09:52:31', '2017-03-29 15:22:31', 'Product Description !!', 'Active', 0),
(11, 1008, '25 mm wall 1 W', '25 mm wall 1 W', 2, 83, 221, 'nos', '390', '13', '5070', '2017-03-29 10:00:11', '2017-03-29 15:30:11', 'Product Description !!', 'Active', 1),
(12, 1008, '25 mm wall 2W', '25 mm wall 2W', 2, 83, 222, 'nos', '300', '13.65', '4095', '2017-03-29 10:06:20', '2017-03-29 15:36:20', 'Product Description !!', 'Active', 0),
(13, 1008, '25 mm wall 2W', '25 mm wall 2W', 2, 83, 222, 'nos', '300', '13.65', '4095', '2017-03-29 10:06:26', '2017-03-29 15:36:26', 'Product Description !!', 'Active', 1),
(14, 1008, '25 mm wall 3W', '25 MM WALL 3W', 2, 83, 223, 'nos', '300', '14.95', '4485', '2017-03-29 10:11:38', '2017-03-29 15:41:38', 'Product Description !!', 'Active', 1),
(15, 1008, '25 MM WALL 4W', '25 mm wall 4W', 2, 83, 224, 'nos', '990', '9.43', '9335.7', '2017-03-29 10:17:30', '2017-03-29 15:47:30', 'Product Description !!', 'Active', 1),
(16, 1008, '20 mm wall 4W', '20 mm wall 4W', 2, 83, 225, 'nos', '100', '12.35', '1235', '2017-03-29 10:20:14', '2017-03-29 15:50:14', 'Product Description !!', 'Active', 1),
(17, 1008, 'Fan Box(Heavy)', 'Fan Box(Heavy)', 2, 39, NULL, 'nos', '290', '75', '21750', '2017-03-29 10:23:54', '2017-07-05 11:16:10', 'Product Description !!', 'Active', 1),
(18, 1008, '25 mm pvc bend(medium)', '25 mm pvc bend', 2, 41, 226, 'nos', '3950', '9.10', '35945', '2017-03-29 10:26:48', '2017-07-05 11:15:38', 'Product Description !!', 'Active', 1),
(19, 1008, '20 mm pvc bend(medium)', '20 mm pvc bend(medium)', 2, 41, 227, 'nos', '2040', '5.5', '11220', '2017-03-29 10:29:33', '2017-07-05 11:15:01', 'Product Description !!', 'Active', 1),
(20, 1008, 'PVC Tape RED', 'PVC Tape RED', 2, 84, 228, 'nos', '60', '8.5', '510', '2017-03-29 10:40:48', '2017-03-29 16:10:48', 'Product Description !!', 'Active', 1),
(21, 1008, 'PVC Tape YELLOW', 'PVC Tape YELLOW', 2, 84, 229, 'nos', '60', '8.5', '510', '2017-03-29 10:41:19', '2017-03-29 16:11:19', 'Product Description !!', 'Active', 1),
(22, 1008, 'PVC Tape YELLOW', 'PVC Tape YELLOW', 2, 84, 229, 'nos', '60', '8.5', '510', '2017-03-29 10:41:23', '2017-03-29 16:11:23', 'Product Description !!', 'Active', 0),
(23, 1008, 'PVC Tape BLUE', 'PVC Tape BLUE', 2, 84, 230, 'nos', '60', '8.5', '510', '2017-03-29 10:41:56', '2017-03-29 16:11:56', 'Product Description !!', 'Active', 1),
(24, 1008, 'PVC Tape BLACK', 'PVC Tape BLACK', 2, 84, 231, 'nos', '60', '8.5', '510', '2017-03-29 10:45:59', '2017-03-30 17:51:49', 'Product Description !!', 'Active', 1),
(25, 1008, 'PVC Tape GREEN', 'PVC Tape GREEN', 2, 84, 232, 'nos', '60', '8.5', '510', '2017-03-29 10:46:52', '2017-03-29 17:23:20', 'Product Description !!', 'Active', 1),
(26, 1008, 'Steel wire', 'Steel wire', 2, 87, NULL, 'kg', '25', '75', '1875', '2017-06-10 03:25:38', '2017-06-10 08:55:38', 'Product Description !!', 'Active', 1),
(27, 1008, 'G.I CONDUIT PIPE', 'G.I Conduit ', 2, 38, 190, 'nos', '26', '700', '18200', '2017-06-10 03:26:55', '2017-06-10 08:56:55', 'Product Description !!', 'Active', 1),
(28, 1008, 'C.I HAUDI WITH COVER', 'C.I HAUDI COVER', 2, 88, NULL, 'nos', '26', '335', '8710', '2017-06-10 03:30:19', '2017-06-10 09:00:19', 'Product Description !!', 'Active', 1),
(29, 1008, 'G.I PLATE 600X600X6MM', '600X600X6MM', 2, 89, NULL, 'nos', '26', '1175', '30550', '2017-06-10 03:31:55', '2017-06-10 09:01:55', 'Product Description !!', 'Active', 1),
(30, 1008, 'JUNCTION BOX LEAD & SCREW PRECISION', 'JUNCTION BOX LEAD & SCREW ', 2, 90, NULL, 'nos', '2100', '2.93', '6153', '2017-06-10 03:34:01', '2017-06-10 09:04:01', 'Product Description !!', 'Active', 1),
(31, 1008, 'FLUSH BOX 4X2 MODULE', 'FLUSH BOX 4X2 ', 2, 91, NULL, 'nos', '89', '118', '10502', '2017-06-10 03:36:20', '2017-06-10 09:06:20', 'Product Description !!', 'Active', 1),
(32, 1008, 'FLUSH BOX 3 MODULE', 'FLUSH BOX 3 MODULE', 2, 91, 240, 'nos', '855', '49.5', '42322.5', '2017-06-10 03:37:28', '2017-07-05 10:53:53', 'Product Description !!', 'Active', 1),
(33, 1008, 'FLUSH BOX 1/2 MODULE', 'FLUSH BOX 1/2 MODULE', 2, 91, 241, 'nos', '245', '49.5', '12127.5', '2017-06-10 03:38:17', '2017-07-05 10:53:18', 'Product Description !!', 'Active', 1),
(34, 1008, 'ETPN 12 W', 'ETPN 12 W', 2, 92, NULL, 'nos', '2', '4426', '8852', '2017-06-10 03:40:45', '2017-06-10 09:10:45', 'Product Description !!', 'Active', 1),
(35, 1008, 'SPN 8 W', 'SPN 8 W', 2, 93, NULL, 'nos', '5', '1013', '5065', '2017-06-10 03:41:57', '2017-06-10 09:11:57', 'Product Description !!', 'Active', 1),
(36, 1008, '20 mm pvc socket', '20mm pvc socket', 2, 43, 242, 'nos', '1000', '2.93', '2930', '2017-07-05 05:35:57', '2017-07-05 11:05:57', 'Product Description !!', 'Active', 1),
(37, 1008, '25 mm pvc socket', '25mm pvc socket', 2, 43, 243, 'nos', '1000', '4.23', '4230', '2017-07-05 05:36:36', '2017-07-05 11:06:36', 'Product Description !!', 'Active', 1),
(38, 1008, '25 mm pvc socket', '25mm pvc socket', 2, 43, 243, 'nos', '1000', '4.23', '4230', '2017-07-05 05:36:43', '2017-07-05 11:06:43', 'Product Description !!', 'InActive', 0),
(39, 1008, '6 Amp switch', '6 Amp switch', 2, 94, 244, 'nos', '60', '59', '3540', '2017-07-05 05:49:43', '2017-07-05 11:19:43', 'Product Description !!', 'Active', 1),
(40, 1008, '5/6 Amp modular socket outlet ', '5/6 Amp modular socket outlet', 2, 50, NULL, 'nos', '20', '109', '2180', '2017-07-05 05:51:35', '2017-07-05 11:21:35', 'Product Description !!', 'Active', 1),
(41, 1008, '16 Amp modular switch ', '16 Amp modular switch', 2, 48, NULL, 'nos', '20', '102', '2040', '2017-07-05 05:53:28', '2017-07-05 11:23:28', 'Product Description !!', 'Active', 1),
(42, 1008, '16 Amp modular socket', '16 Amp modular socket', 2, 95, NULL, 'nos', '20', '150', '3000', '2017-07-05 05:54:59', '2017-07-05 11:24:59', 'Product Description !!', 'Active', 1),
(43, 1008, 'modular fan regulator step type', 'modular fan regulator step type', 2, 52, 239, 'nos', '10', '335', '3350', '2017-07-05 05:56:45', '2017-07-05 11:26:45', 'Product Description !!', 'Active', 1),
(44, 1008, '6 Amp Bell push', '6 Amp Bell push', 2, 51, NULL, 'nos', '2', '86', '172', '2017-07-05 06:06:47', '2017-07-05 11:36:47', 'Product Description !!', 'Active', 1),
(45, 1008, 'Buzzer anchor', 'Buzzer anchor', 2, 96, NULL, 'nos', '2', '45', '90', '2017-07-05 06:08:05', '2017-07-05 11:38:05', 'Product Description !!', 'Active', 1),
(46, 1008, 'Telephone socket(RJ11)', 'Telephone socket(RJ11)', 2, 34, NULL, 'nos', '4', '75', '300', '2017-07-05 06:09:34', '2017-07-05 11:39:34', 'Product Description !!', 'Active', 1),
(47, 1008, 'TV socket', 'TV socket', 2, 35, NULL, 'nos', '4', '76', '304', '2017-07-05 06:11:20', '2017-07-05 11:41:20', 'Product Description !!', 'Active', 1),
(48, 1008, 'Telephone Tag Block 10 pair-krone', 'Telephone Tag block 10 Pair-krone', 2, 33, 0, 'nos', '1', '220', '220', '2017-07-05 06:12:05', '2017-07-05 11:42:05', 'Product Description !!', 'Active', 1),
(49, 1008, '1 modular switch plate', '1 modular switch plate', 2, 97, NULL, 'nos', '15', '52', '780', '2017-07-05 06:15:38', '2017-07-05 11:45:38', 'Product Description !!', 'Active', 1),
(50, 1008, '2 modular switch plate', '2 modular switch plate', 2, 98, NULL, 'nos', '1', '55', '55', '2017-07-05 06:16:43', '2017-07-05 11:46:43', 'Product Description !!', 'Active', 1),
(51, 1008, '3 modular switch plate', '3 modular switch plate', 2, 99, NULL, 'nos', '31', '60', '1860', '2017-07-05 06:18:07', '2017-07-05 11:48:07', 'Product Description !!', 'Active', 1),
(52, 1008, '6 modular switch plate', '6 modular switch plate', 2, 100, NULL, 'nos', '2', '116', '232', '2017-07-05 06:19:21', '2017-07-05 11:49:21', 'Product Description !!', 'Active', 1),
(53, 1008, '8 modular switch plate', '8 modular switch plate', 2, 101, NULL, 'nos', '5', '158', '790', '2017-07-05 06:20:12', '2017-07-05 11:50:12', 'Product Description !!', 'Active', 1),
(54, 1008, 'Exhaust Fan Louver(12")', 'Exhaust Fan Louver(12")', 2, 102, NULL, 'nos', '4', '140', '560', '2017-07-05 06:25:47', '2017-07-05 11:55:47', 'Product Description !!', 'Active', 1),
(55, 1008, '12" Exhaust Fan', '12" Exhaust Fan', 2, 103, NULL, 'nos', '4', '2150', '8600', '2017-07-05 06:27:39', '2017-07-05 11:57:39', 'Product Description !!', 'Active', 1),
(56, 1008, 'LED Elite Green 18 Watt 6K', 'LED Elite Green 18 Watt 6K', 2, 104, NULL, 'nos', '6', '350', '2100', '2017-07-05 06:29:27', '2017-07-05 11:59:27', 'Product Description !!', 'Active', 1),
(57, 1008, '20 AMP AC SP DB', '20 Amp AC SP DB', 2, 105, NULL, 'nos', '48', '869.50', '41736', '2017-07-05 06:32:38', '2017-07-05 12:02:38', 'Product Description !!', 'InActive', 0),
(58, 1008, 'DBsdfadsfsd', '20 Amp AC SP DB', 2, 105, NULL, 'nos', '48', '869.50', '41736', '2017-07-05 06:32:46', '2017-07-05 12:02:46', 'Product Description !!', 'Active', 1),
(59, 1008, 'sdfadasf', '3.5 core 150 sqmm AL Armoured cable', 1, 8, 53, 'mtrs', '1074', '444.38', '477264.12', '2017-07-06 06:30:08', '2017-07-06 12:01:39', 'Product Description !!', 'Active', 1),
(60, 1008, 'Baisen', '3.5 core 70 sqmm AL Armoured cable', 1, 8, 50, 'mtrs', '309', '240.75', '74391.75', '2017-07-06 06:32:58', '2017-07-06 12:04:05', 'Product Description !!', 'Active', 1),
(61, 1008, 'Rice', '3.5 core 50 sqmm AL Armoured cable', 2, NULL, NULL, 'kg', '3000', '80', '240000', '2017-07-06 06:35:08', '2018-06-26 10:52:06', 'Product Description !!', 'Active', 1),
(62, 1008, 'Atta Rajdhani', '3.5 core 35 sqmm AL Armoured cable', 3, NULL, NULL, 'kg', '40', '138.38', '5535.2', '2017-07-06 06:36:00', '2018-06-26 10:51:27', 'Product Description !!', 'Active', 1),
(63, 1008, 'Pista ', '3.5 core 120 sqmm AL Armoured cable', 3, NULL, NULL, 'meter', '500', '374.63', '187315', '2017-07-06 06:37:06', '2018-06-26 10:51:41', 'Product Description !!', 'Active', 1),
(64, 1008, 'Kismis PL', '3.5 core 25 sqmm AL Armoured cable', 2, NULL, NULL, 'kg', '45', '800', '36000', '2017-07-06 06:37:53', '2018-06-26 10:50:30', 'Product Description !!', 'Active', 1),
(65, 1008, 'California Almonds ', '2 core 6 sqmm AL Armoured cable', 2, NULL, NULL, 'kg', '107', '48.38', '5176.66', '2017-07-06 06:40:07', '2018-06-26 10:50:01', 'Product Description !!', 'Active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_products`
--

CREATE TABLE IF NOT EXISTS `site_products` (
  `siteProductId` int(11) NOT NULL AUTO_INCREMENT,
  `siteInChargeId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `issuedQuantity` int(11) NOT NULL,
  `pricePerUnit` int(11) NOT NULL,
  `totalPrice` int(11) NOT NULL,
  `insertDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime NOT NULL,
  PRIMARY KEY (`siteProductId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `site_products`
--

INSERT INTO `site_products` (`siteProductId`, `siteInChargeId`, `productId`, `issuedQuantity`, `pricePerUnit`, `totalPrice`, `insertDate`, `updateDate`) VALUES
(20, 1014, 5, 56, 180, 10080, '2016-12-31 22:16:41', '2017-01-01 03:46:41'),
(21, 1014, 3, 1, 24000, 24000, '2016-12-31 22:16:41', '2017-01-01 03:46:41'),
(22, 1010, 25, 1, 9, 9, '2017-03-30 12:15:23', '2017-03-30 17:45:23'),
(23, 1010, 65, 100, 48, 4838, '2017-10-06 10:51:50', '2017-10-06 10:51:50'),
(24, 1011, 61, -121, 178, -21508, '2017-10-06 10:56:11', '2017-10-06 10:56:11'),
(25, 1010, 65, 45, 48, 2177, '2018-05-11 14:22:28', '2018-05-11 19:52:28'),
(26, 1010, 64, 67, 111, 7462, '2018-05-11 14:22:28', '2018-05-11 19:52:28'),
(27, 1010, 63, 99, 375, 37088, '2018-05-11 14:22:28', '2018-05-11 19:52:28'),
(28, 1011, 65, 39, 48, 1887, '2018-05-31 12:24:27', '2018-05-31 17:54:27'),
(29, 1011, 63, 100, 375, 37463, '2018-05-31 12:24:27', '2018-05-31 17:54:27'),
(30, 1011, 61, 400, 178, 71100, '2018-05-31 12:24:27', '2018-05-31 17:54:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `emailId` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `officeLandLine` varchar(45) DEFAULT 'NULL',
  `status` varchar(45) NOT NULL,
  `privilege` varchar(45) NOT NULL,
  `userToken` varchar(45) NOT NULL COMMENT 'MD5 of user Email Id',
  `insertDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `middleName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `gender` varchar(11) DEFAULT NULL,
  `profile` varchar(45) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT 'India',
  `pincode` int(6) DEFAULT NULL,
  `dob` varchar(10) DEFAULT NULL,
  `delete_flag` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1016 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `emailId`, `password`, `mobile`, `officeLandLine`, `status`, `privilege`, `userToken`, `insertDate`, `updateDate`, `firstName`, `middleName`, `lastName`, `gender`, `profile`, `address1`, `address2`, `city`, `state`, `country`, `pincode`, `dob`, `delete_flag`) VALUES
(1000, 'surendra.vimal27@gmail.com', '09it566gitm', '9560093160', '012442227233', 'Active', '99', 'asfhkashkffkjduys87f8dsdfds', '2016-10-10 17:32:30', '2016-12-12 21:56:00', 'Surendra', 'Kumar', 'Vimal', 'Male', 'WebMaster', 'DLF Phase 3', 'U Block', 'Gugaon', 'Haryana', 'India', 122001, '15/09/1992', b'1'),
(1007, 'niwas_bhontele@yahoo.com', 'niwas2409', '9910704214', '1122322223', 'Active', '99', '4754b5d6bd86f6bfc32eaeadcd51705a', '2016-12-08 02:57:59', '2016-12-22 17:08:08', 'Ramniwas', '', 'Bhontele', 'Male', 'Admin', 'New Delhi', 'RMS CONSTRUCTION PVT.LTD. VASANT KUNJ NEW DELHI', 'New Delhi', 'Delhi', 'India', 110070, '24/09/1972', b'1'),
(1008, 'dhrub_kr1988@rediffmail.com', 'firewall', '9811718542', '01126125386', 'Active', '51', '29ae9c8f7071f40bfe38287186663ad1', '2016-12-08 04:33:16', '2017-03-29 17:25:40', 'Dhrub ', '', 'Kumar', 'Male', 'Electrical Engineer', 'RMS Construction Pvt.Ltd.', 'Vasant Kunj ', 'New Delhi', 'Delhi', 'India', 110070, '13/06/1988', b'1'),
(1009, 'ravikushwaha1977@gmail.com', 'flBWk7', '9454141258', '', 'InActive', '1', '3f3e4f827e52e0eedfce266a09aeba81', '2016-12-10 10:46:39', '2016-12-10 16:16:39', 'Ravikant', '', 'Kushwah', 'Male', '', 'Jalandhar', 'Punjab', 'Jalandhar', 'Punjab', 'India', 144001, '', b'1'),
(1010, 'kumar.sachin203@gmail.com', 'Q4yItV', '7424940998', '', 'Active', '1', '62fade949e385711ef32197593b49334', '2016-12-10 11:02:03', '2017-03-29 17:13:50', 'Sachin ', '', 'Kumar', 'Male', '', 'Defence Laboratory', 'Ratanada Palace', 'Jodhpur', 'Rajasthan', 'India', 342001, '', b'1'),
(1011, 'manoj.purohit05@gmail.com', 'ZhbQzp', '8441012367', '', 'Active', '1', 'ca2f943ab149097e540863dbbbd991ff', '2016-12-10 11:06:30', '2017-03-30 17:48:56', 'Roop ', '', 'Singh', 'Male', '', 'Shobhasar', 'Army area', 'Bikaner', 'Rajasthan', 'India', 331801, '', b'1'),
(1012, 'rmsanilrai@gmail.com', 'RTCYXs', '9450872415', '', 'InActive', '1', 'c441b76768aca8ce19cba24edeeaa716', '2016-12-10 11:11:21', '2016-12-10 16:41:21', 'Anil ', '', 'Rai', 'Male', '', 'DRDO Project site', 'Teli ka bagh', 'Lucknow', 'U P', 'India', 226005, '', b'1'),
(1013, 'rms_delhi@yahoo.co.in', 'i5TNj6', '9910704214', '01126125387', 'Active', '51', '9c32bc1723f364dc7d7fd052765efde4', '2016-12-10 11:15:44', '2016-12-10 16:45:44', 'Ramniwas', '', 'Bhontele', 'Male', '', 'Sector-B,Pocket-1,Plot No-1,1st Floor, ', 'Local Shopping Complex Vasant Kunj', 'New Delhi', 'Delhi', 'India', 110070, '24/09/1972', b'1'),
(1014, 'verma_rel@yahoo.co.in', '230977', '8769524600', '8769524600', 'Active', '1', 'd3a0f3254afc3b7984e54c0ac86a42a7', '2016-12-10 11:18:43', '2016-12-24 15:32:30', 'Sanjeev', '', 'Kumar', 'Male', '', 'Village -Indor', 'Tapukara', 'Alwar', 'Rajasthan', 'India', 301707, '22/08/1976', b'1'),
(1015, 'manojpurohit05@gmail.com', 'BIKANER05', '9928812179', '', 'Active', '1', '8be9442d245ec998cbe811b902d37b9b', '2017-04-01 09:16:51', '2017-04-01 14:46:51', 'Manoj ', '', 'Purohit', 'Male', '', 'Bikaner', '', 'Bikaner', 'Rajasthan', 'India', 332021, '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE IF NOT EXISTS `user_logs` (
  `logId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `loginTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `loginIPAddress` varchar(45) NOT NULL,
  `loginLocation` varchar(100) DEFAULT NULL,
  `loginBrowser` varchar(150) DEFAULT NULL,
  `loginDevice` varchar(100) DEFAULT NULL,
  `loginPrivilege` varchar(45) NOT NULL,
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `userTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `privilegeId` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`userTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`userTypeId`, `privilegeId`, `name`) VALUES
(1, 99, 'SuperAdmin'),
(2, 51, 'SubAdmin'),
(3, 1, 'Site InCharge');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
